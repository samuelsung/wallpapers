{ config, lib, ... }:

let
  inherit (lib)
    attrValues
    concatMapStringsSep
    mapAttrs
    flip
    filterAttrs
    foldr;
  inherit (config.helpers.overrides) waifu2x;
in
{
  moe2nix.lockFile = ../moe2nix.lock;

  perSystem = { config, pkgs, ... }:
    let
      imagePath = original: image:
        if original
        then image.file
        else ((waifu2x { } pkgs) image).file;

      mkWallpapers = name: { original ? false, filters ? [ ] }:
        let
          files = if original then config.moe2nix.files-original else config.moe2nix.files;
          list = flip filterAttrs files (_: image: foldr (cur: acc: (cur image) && acc) true filters);
        in
        pkgs.runCommand name { passthru.files = mapAttrs (_: imagePath original) list; } ''
          mkdir $out
          ${concatMapStringsSep "\n"
            (image: "cp \"${imagePath original image}\" \"\${out}/${(imagePath original image).name}\"")
            (attrValues list)}
        '';
    in
    {
      config.packages.wallpapers = mkWallpapers "wallpapers" { };

      config.packages.wallpapers-original = mkWallpapers "wallpapers-original" { original = true; };

      config.packages.wallpapers-sfw = mkWallpapers "wallpapers-sfw" { filters = [ ({ rating, ... }: rating == "sfw") ]; };

      config.packages.wallpapers-nsfw = mkWallpapers "wallpapers-nsfw" { filters = [ ({ rating, ... }: rating == "nsfw") ]; };
    };
}
