{ inputs, lib, ... }:

let
  inherit (lib) makeBinPath;
in
{
  imports = [ inputs.devenv.flakeModule ];
  perSystem = { pkgs, inputs', ... }: {
    devenv.shells.default = {
      packages = [
        pkgs.gimp
        inputs'.notalike.packages.notalike
      ];

      scripts.posts-danbooru.exec = ''
        tag=$1
        page=$2

        PATH=${makeBinPath [ pkgs.curl pkgs.jq ]}

        json=$(curl \
        --user-agent 'moe2nix/0.0.1' \
        "https://danbooru.donmai.us/posts.json?page=$page&tags=$tag")

        echo "" >&2
        echo "" >&2

        echo "$json" | jq -r '.[] | select(.media_asset | has("variants")) | ("rating: " + .rating + (if .has_children == true then ", has_children" else "" end) + (if .parent_id != null then (", parent_id: " + (.parent_id | tostring)) else "" end)), .id, "{ id = " + (.id | tostring) + "; overrides = [ (crop { height = " + (.image_height | tostring) + "; }) ]; }", "{ id = " + (.id | tostring) + "; overrides = [ (crop { height = " + (.image_height * 3 / 4 | ./50 | round | .*50 | tostring) + "; }) ]; }", "{ id = " + (.id | tostring) + "; overrides = [ (crop { width = "+ (.image_width | tostring) +"; height = " + (.image_height | tostring) + "; left = 0; top = 0; }) ]; }", ""'
      '';

      scripts."posts-yande.re".exec = ''
        tag=$1
        page=$2

        PATH=${makeBinPath [ pkgs.curl pkgs.jq ]}

        json=$(curl \
        --user-agent 'moe2nix/0.0.1' \
        "https://yande.re/post.json?page=$page&tags=$tag")

        echo "" >&2
        echo "" >&2

        echo "$json" | jq -r '.[] | ("rating: " + .rating + (if .has_children == true then ", has_children" else "" end) + (if .parent_id != null then (", parent_id: " + (.parent_id | tostring)) else "" end)), .id, "{ id = " + (.id | tostring) + "; overrides = [ (crop { height = " + (.height | tostring) + "; }) ]; }", "{ id = " + (.id | tostring) + "; overrides = [ (crop { height = " + (.height * 3 / 4 | ./50 | round | .*50 | tostring) + "; }) ]; }", "{ id = " + (.id | tostring) + "; overrides = [ (crop { width = "+ (.width | tostring) +"; height = " + (.height | tostring) + "; left = 0; top = 0; }) ]; }", ""'
      '';
    };
  };
}
