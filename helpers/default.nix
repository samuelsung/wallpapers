{ lib, ... }:

let
  inherit (lib)
    concatStrings
    concatStringsSep
    optionalString
    mkOption
    foldl;
  inherit (builtins) ceil;
  inherit (lib.types) attrsOf anything;

  max = a: b: if a > b then a else b;
in
{
  options.helpers.overrides = mkOption {
    type = attrsOf anything;
  };

  config.helpers.overrides = {
    rename = name: pkgs: image: image // {
      inherit name;
      file = pkgs.runCommand "${name}.${image.extension}" { } ''
        cp ${image.file} $out
      '';
    };

    format = extension: pkgs: image:
      image // {
        inherit extension;
        file = pkgs.runCommand "${image.name}.${extension}" { } ''
          ${pkgs.imagemagick}/bin/convert ${image.file} $out
        '';
      };

    rect = opt: pkgs: image:
      let
        color = opt.color or "none";

        width = opt.width or image.width;
        height = opt.height or image.height;
        left = opt.left or 0;
        top = opt.top or 0;

        right = left + width;
        bottom = top + height;


        name = "${image.name}-rect-${color}-${toString width}-${toString height}-${toString left}-${toString top}";
      in
      image // {
        inherit name;
        file = pkgs.runCommand "${name}.${image.extension}" { } ''
          ${pkgs.imagemagick}/bin/convert ${image.file} -fill '${color}' -draw "rectangle ${toString left},${toString top} ${toString right},${toString bottom}" $out
        '';
      };


    fill = opt: pkgs: image:
      let
        background = opt.background or "none";
        fuzz = opt.fuzz or 10;
        name = "${image.name}-fill-${background}-${toString fuzz}";
      in
      foldl
        (acc: cur:
          let
            x = cur.x or 0;
            y = cur.y or 0;
          in
          acc // {
            inherit name;
            file = pkgs.runCommand "${name}.${image.extension}" { } ''
              color=$( ${pkgs.imagemagick}/bin/convert ${acc.file} -format "%[pixel:p{${toString x},${toString y}}]" info:- )
              ${pkgs.imagemagick}/bin/convert ${acc.file} -bordercolor $color -border 1 \
                  \( +clone -fuzz ${toString fuzz}% -fill none -floodfill +${toString x}+${toString y} $color \
                    -alpha extract -geometry 200% -blur 0x0.5 \
                    -morphology erode square:1 -geometry 50% \) \
                  -compose CopyOpacity -composite -shave 1 transparent.png

              ${pkgs.imagemagick}/bin/convert transparent.png -background '${background}' -flatten $out
            '';
          })
        image
        (opt.starts or [ ]);

    extent = opt: pkgs: image:
      let
        background = opt.background or "none";
        gravity = opt.gravity or "center";
        width = opt.width or image.width;
        height = opt.height or image.height;

        dimension = "${toString width}x${toString height}";
        name = "${image.name}-extent-${background}-${gravity}-${dimension}";
      in
      image // {
        inherit height width name;
        file = pkgs.runCommand "${name}.${image.extension}" { } ''
          ${pkgs.imagemagick}/bin/convert ${image.file} -background '${background}' -gravity ${gravity} -extent ${dimension} $out
        '';
      };

    crop = opt: pkgs: image:
      let
        width = opt.width or image.width;
        height = opt.height or image.height;
        left = opt.left or 0;
        top = opt.top or 0;

        dimension = "${toString width}x${toString height}+${toString left}+${toString top}";
        name = "${image.name}-${dimension}";
      in
      image // {
        inherit height width name;
        file = pkgs.runCommand "${name}.${image.extension}" { } ''
          ${pkgs.imagemagick}/bin/convert ${image.file} -crop ${dimension} $out
        '';
      };

    rotate = degree: pkgs: image:
      let
        height = if (degree == 0 || degree == 180) then image.height else image.width;
        width = if (degree == 0 || degree == 180) then image.width else image.height;
        name = "${image.name}-rotate-${toString degree}";
      in
      lib.checkListOfEnum "rotate degree" [ 0 90 180 270 ] [ degree ]
        (image // {
          inherit height width name;
          file = pkgs.runCommand "${name}.${image.extension}" { } ''
            ${pkgs.imagemagick}/bin/convert ${image.file} -rotate ${toString degree} $out
          '';
        });

    sfw = _: image:
      image // {
        rating = "sfw";
      };

    nsfw = _: image:
      image // {
        rating = "nsfw";
      };

    waifu2x = opts: pkgs: image:
      let
        widthScale = ceil (3840.0 / image.width);
        heightScale = ceil (2160.0 / image.height);
        scale = opts.scale or (max widthScale heightScale);

        shouldScaleUp = scale > 1;
        shouldReduceNoise = opts.noise or (image.extension == "jpg");

        args =
          if (shouldScaleUp && shouldReduceNoise)
          then "-m noise-scale --noise-level 3 --scale-ratio ${toString scale}"
          else if shouldScaleUp
          then "-m scale --scale-ratio ${toString scale}"
          else if shouldReduceNoise
          then "-m noise --noise-level 3"
          else "";

        name = concatStrings [
          "${image.name}-waifu2x"
          (optionalString shouldScaleUp "-scale-${toString scale}")
          (optionalString shouldReduceNoise "-noise-3")
        ];

        output = pkgs.runCommand
          "${name}.png"
          { }
          ''
            ${pkgs.waifu2x-converter-cpp}/bin/waifu2x-converter-cpp ${args} -i ${image.file} -o $out
          '';

      in
      if (!shouldScaleUp && !shouldReduceNoise)
      then image
      else
        image // {
          inherit name;
          file = output;
          extension = "png";
          width = scale * image.width;
          height = scale * image.height;
        };
  };
}
