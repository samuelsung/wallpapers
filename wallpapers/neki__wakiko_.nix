# わき
# [danbooru - neki(wakiko)](https://danbooru.donmai.us/artists/66713)
# [yande.re - waki_(pixiv2609622)](https://yande.re/wiki/show?title=waki_%28pixiv2609622%29)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop sfw;
in
{
  config.moe2nix.danbooru = [
    5857984
    5289119
    5267753
    5225238
    5142227
    3658766 # yande.re 582955
    3644659
    { id = 3567550; overrides = [ sfw ]; } # yande.re 552831
    { id = 3537009; overrides = [ sfw (crop { height = 3000; top = 270; }) ]; }
    3449634
    3345020
    { id = 3341611; overrides = [ sfw ]; }
    3341447
    3339448
    3330966
    { id = 3064882; overrides = [ sfw (crop { height = 3200; }) ]; } # yande.re 500289
    2942545
    { id = 2930545; overrides = [ sfw ]; } # yande.re 420202
    2809331
    { id = 2659509; overrides = [ (crop { height = 2400; top = 150; }) ]; } # yande.re 500286
    { id = 2551808; overrides = [ sfw ]; }
    { id = 2279879; overrides = [ sfw ]; } # yande.re 347751
    2257613
    { id = 2251368; overrides = [ sfw ]; } # yande.re 344724
    { id = 2247500; overrides = [ sfw ]; } # yande.re 344552
    2241403 # yande.re 343731
    { id = 2224388; overrides = [ sfw ]; } # yande.re 341985
    { id = 2208393; overrides = [ sfw ]; }
    2178421 # yande.re 337222
    { id = 2152335; overrides = [ sfw ]; } # yande.re 334148
    { id = 1857401; overrides = [ sfw (crop { height = 690; }) ]; }
    1173612
    939373
  ];

  config.moe2nix."yande.re" = [
    { id = 621549; overrides = [ (crop { width = 4677; left = 220; }) ]; } # danbooru 5070206
  ];
}
