# 桃豆こまもち
# - [danbooru - momozu komamochi](https://danbooru.donmai.us/artists/193590)
# - [yande.re - momozu komamochi](https://yande.re/wiki/show?title=momozu_komamochi)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop rotate sfw nsfw waifu2x;
in
{
  config.moe2nix.danbooru = [
    { id = 8509138; overrides = [ (crop { height = 1100; }) ]; }
    8276019
    { id = 8267689; overrides = [ (crop { height = 1400; }) ]; }
    { id = 8227237; overrides = [ (crop { height = 800; }) ]; }
    { id = 8192601; overrides = [ (crop { height = 900; }) ]; } # yande.re 1197233
    { id = 8150867; overrides = [ sfw (crop { height = 1000; }) ]; }
    { id = 8087123; overrides = [ (crop { height = 900; }) ]; }
    { id = 8066495; overrides = [ sfw (crop { height = 900; }) ]; }
    { id = 8066416; overrides = [ sfw (crop { height = 2200; }) ]; } # yande.re 947371
    { id = 8066369; overrides = [ (crop { height = 1100; }) ]; } # yande.re 1058977
    7895003 # yande.re 1197232
    { id = 7845849; overrides = [ (crop { height = 900; }) ]; } # yande.re 1197237
    { id = 7438399; overrides = [ (rotate 90) ]; }
    { id = 7405806; overrides = [ (crop { height = 900; }) ]; } # yande.re 1197235
    7225667
    { id = 6954978; overrides = [ sfw (crop { height = 900; }) ]; }
    { id = 6887321; overrides = [ (crop { height = 900; }) ]; }
    { id = 6828627; overrides = [ (crop { height = 1400; }) ]; }
    { id = 6826876; overrides = [ (crop { height = 1500; }) ]; } # yande.re 1121940
    { id = 6707474; overrides = [ (crop { height = 900; }) ]; }
    { id = 6636444; overrides = [ (crop { height = 1100; }) ]; } # yande.re 1121938
    { id = 6265449; overrides = [ (crop { height = 1100; }) ]; } # yande.re 1121934
    { id = 6224994; overrides = [ (crop { height = 1100; }) ]; }
    { id = 6224973; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5933048; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5928418; overrides = [ (crop { height = 1100; }) ]; } # yande.re 1058954
    { id = 5848940; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5771652; overrides = [ (crop { height = 1300; }) ]; }
    { id = 5720803; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5650847; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5630192; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5573755; overrides = [ (crop { height = 1100; }) ]; } # yande.re 1058961
    { id = 5510854; overrides = [ (crop { height = 1500; }) ]; } # yande.re 1058968
    { id = 5462528; overrides = [ (crop { height = 1300; }) ]; } # yande.re 1058970
    { id = 5462525; overrides = [ (crop { height = 1300; }) ]; } # yande.re 1058969
    { id = 5458254; overrides = [ (crop { height = 1300; }) ]; } # yande.re 990887
    { id = 5323452; overrides = [ (crop { height = 900; }) ]; }
    { id = 5183731; overrides = [ (crop { height = 900; }) ]; }
    { id = 5177240; overrides = [ (crop { height = 1500; }) ]; } # yande.re 959065
    { id = 5174509; overrides = [ sfw (crop { height = 1100; }) ]; }
    { id = 5174505; overrides = [ (crop { height = 1100; }) ]; } # yande.re 959060
    { id = 5174475; overrides = [ (crop { height = 1100; }) ]; } # yande.re 959052
    { id = 5174215; overrides = [ (crop { height = 1300; }) ]; } # yande.re 959050
    { id = 5173950; overrides = [ sfw ]; } # yande.re 959047
    { id = 5173726; overrides = [ (crop { height = 1100; }) ]; } # yande.re 937948
    { id = 5165191; overrides = [ (crop { height = 1500; }) ]; } # yande.re 1058978
    { id = 5165182; overrides = [ (crop { height = 1100; }) ]; } # yande.re 959046
    { id = 5149069; overrides = [ (crop { height = 1700; }) ]; } # yande.re 959048
    5148436 # yande.re 932924
    { id = 5139244; overrides = [ sfw (crop { height = 1300; }) ]; } # yande.re 870441
    { id = 5098071; overrides = [ (crop { height = 1000; }) ]; }
    { id = 5098032; overrides = [ sfw (crop { height = 1200; }) ]; } # yande.re 959049
    { id = 4955393; overrides = [ (crop { height = 1300; }) ]; }
    { id = 4955367; overrides = [ (crop { height = 1400; }) ]; } # yande.re 893133
    { id = 4913131; overrides = [ (crop { height = 1600; }) ]; } # yande.re 883344
    4888846 # yande.re 943286
    { id = 4881656; overrides = [ (crop { height = 1300; }) ]; } # yande.re 959056
    { id = 4840042; overrides = [ (crop { height = 1500; }) ]; } # yande.re 959063
    { id = 4824563; overrides = [ (crop { height = 1300; }) ]; }
    { id = 4813801; overrides = [ (crop { height = 1200; }) ]; } # yande.re 857016
    4813795 # yande.re 857015
    { id = 4813596; overrides = [ (crop { height = 1600; }) ]; } # yande.re 959057
    { id = 4804170; overrides = [ (crop { height = 1200; }) ]; } # yande.re 959059
    { id = 4782816; overrides = [ (crop { height = 1200; }) ]; } # yande.re 852071
    { id = 4771808; overrides = [ (crop { height = 1600; }) ]; } # yande.re 959064
    { id = 4749294; overrides = [ (crop { height = 1200; }) ]; }
    { id = 4712087; overrides = [ (crop { height = 1000; }) ]; }
    { id = 4691391; overrides = [ (crop { height = 2200; }) ]; } # yande.re 828641
    { id = 4636426; overrides = [ (crop { height = 1300; }) ]; } # yande.re 857017
    { id = 4612390; overrides = [ (crop { height = 1500; }) ]; } # yande.re 959066
    { id = 4589370; overrides = [ (crop { height = 1400; }) ]; } # yande.re 959062
    { id = 4572702; overrides = [ (crop { height = 2500; }) ]; } # yande.re 959067
    { id = 4558430; overrides = [ (crop { height = 1100; }) ]; }
    { id = 4548755; overrides = [ (crop { height = 1800; }) ]; } # yande.re 959072
    { id = 4531097; overrides = [ (crop { height = 1500; }) ]; }
    { id = 4515668; overrides = [ (crop { height = 1600; }) ]; } # yande.re 959071
    { id = 4493841; overrides = [ (crop { height = 1300; }) ]; } # yande.re 776397
    { id = 4486584; overrides = [ (crop { height = 1300; }) ]; } # yande.re 792117
    { id = 4463427; overrides = [ (crop { height = 1500; }) ]; } # yande.re 792116
    { id = 4446643; overrides = [ (crop { height = 1600; }) ]; }
    { id = 4442297; overrides = [ (crop { height = 2300; }) ]; } # yande.re 792129
    { id = 4435680; overrides = [ sfw (crop { height = 1500; }) ]; }
    { id = 4383515; overrides = [ (crop { height = 1500; }) ]; } # yande.re 748942
    4369413 # yande.re 745107
    { id = 4352155; overrides = [ (crop { height = 1550; }) ]; } # yande.re 741998
    { id = 4339919; overrides = [ (crop { height = 1500; }) ]; } # yande.re 739419
    4329441 # yande.re 737234
    4322997 # yande.re 736150
    { id = 4317386; overrides = [ (crop { height = 1450; }) ]; } # yande.re 734899
    { id = 4276207; overrides = [ (crop { height = 1300; }) ]; } # yande.re 721646
    { id = 4273025; overrides = [ (crop { height = 1400; }) ]; } # yande.re 720994
    { id = 4273022; overrides = [ (crop { height = 1300; }) ]; } # yande.re 720993
    { id = 4267560; overrides = [ (crop { height = 1300; }) ]; } # yande.re 718830
    { id = 4262356; overrides = [ (crop { height = 1300; }) ]; } # yande.re 718378
    { id = 4231359; overrides = [ (crop { height = 1500; }) ]; } # yande.re 712577
    { id = 4172176; overrides = [ sfw (crop { height = 1750; }) ]; } # yande.re 698933
    { id = 4152865; overrides = [ nsfw (crop { height = 1550; }) ]; } # yande.re 694945
    { id = 4130820; overrides = [ (crop { height = 1700; }) ]; } # yande.re 690407
    { id = 4128277; overrides = [ (crop { height = 4400; }) ]; } # yande.re 689377
    { id = 4061164; overrides = [ (crop { height = 1800; }) ]; } # yande.re 675231
    { id = 4049796; overrides = [ sfw (crop { height = 1800; }) ]; } # yande.re 671394
    { id = 4031717; overrides = [ (crop { height = 2450; }) ]; } # yande.re 667667
    { id = 4013080; overrides = [ (crop { height = 2000; }) ]; } # yande.re 792128
    { id = 3997894; overrides = [ (crop { height = 1700; }) ]; } # yande.re 659889
    { id = 3987836; overrides = [ (crop { height = 1950; }) ]; } # yande.re 662122
    { id = 3987803; overrides = [ (crop { height = 2200; }) ]; }
    { id = 3972718; overrides = [ (crop { height = 1800; }) ]; } # yande.re 662114
    { id = 3972700; overrides = [ (crop { height = 1800; }) ]; } # yande.re 662119
    { id = 3972667; overrides = [ (crop { height = 2500; }) ]; } # yande.re 659894
    { id = 3972661; overrides = [ (crop { height = 3400; }) ]; } # yande.re 662124
    { id = 3972645; overrides = [ (crop { height = 2650; }) ]; } # yande.re 659891
    3972639 # yande.re 662127
    { id = 3972190; overrides = [ sfw (crop { height = 1700; }) ]; } # yande.re 662125
    { id = 3951523; overrides = [ (crop { height = 2100; }) ]; } # yande.re 659892
    { id = 3672638; overrides = [ (crop { height = 1800; }) ]; } # yande.re 662120
  ];

  config.moe2nix."yande.re" = [
    { id = 1197236; overrides = [ (crop { height = 2800; }) (waifu2x { noise = true; }) ]; } # danbooru 7421008
    { id = 1197234; overrides = [ nsfw (crop { height = 2600; }) (waifu2x { noise = true; }) ]; } # danbooru 7960401
    { id = 1197231; overrides = [ (crop { height = 2400; }) (waifu2x { noise = true; }) ]; }
    { id = 1197229; overrides = [ sfw (crop { height = 2400; }) (waifu2x { noise = true; }) ]; } # danbooru 7589011
    { id = 1157045; overrides = [ sfw (crop { height = 3300; }) ]; } # danbooru 7272379
    { id = 1140984; overrides = [ (crop { height = 3500; }) ]; } # danbooru 6998026
    { id = 1129744; overrides = [ (crop { height = 3500; }) ]; } # danbooru 6826732
    { id = 1121942; overrides = [ (crop { height = 2567; top = 514; }) (waifu2x { noise = true; }) ]; }
    { id = 1121941; overrides = [ nsfw (crop { height = 2600; }) (waifu2x { noise = true; }) ]; }
    { id = 1121939; overrides = [ (crop { height = 2750; }) (waifu2x { noise = true; }) ]; }
    { id = 1121937; overrides = [ (crop { width = 2296; height = 2000; left = 60; top = 104; }) (waifu2x { noise = true; }) ]; }
    { id = 1121933; overrides = [ (crop { height = 2800; top = 455; }) (waifu2x { noise = true; }) ]; } # danbooru 6488839
    { id = 1121931; overrides = [ nsfw (crop { height = 3124; top = 136; }) (waifu2x { noise = true; }) ]; } # danbooru 6421518
    { id = 1121924; overrides = [ (crop { width = 4881; height = 2656; top = 92; }) (waifu2x { noise = true; }) ]; }
    { id = 1058971; overrides = [ nsfw (crop { height = 3268; top = 108; }) (waifu2x { noise = true; }) ]; }
    { id = 1058966; overrides = [ (crop { height = 2600; }) (waifu2x { noise = true; }) ]; }
    { id = 1058965; overrides = [ (crop { width = 2236; height = 2600; left = 40; top = 104; }) (waifu2x { noise = true; }) ]; }
    { id = 1058964; overrides = [ (crop { width = 2292; height = 2500; left = 68; top = 116; }) (waifu2x { noise = true; }) ]; } # danbooru 5377136
    { id = 1058958; overrides = [ (crop { height = 2500; top = 80; }) (waifu2x { noise = true; }) ]; }
    { id = 1050445; overrides = [ (crop { height = 2600; }) (waifu2x { noise = true; }) ]; }
    { id = 1048463; overrides = [ (crop { height = 1300; }) ]; }
    { id = 1044546; overrides = [ (crop { height = 1200; }) ]; }
    { id = 1024036; overrides = [ (crop { height = 1500; }) ]; } # danbooru 5756853
    { id = 1023209; overrides = [ (crop { height = 2600; }) ]; } # danbooru 5731757
    { id = 1017804; overrides = [ (crop { height = 1500; }) ]; }
    { id = 959051; overrides = [ (crop { width = 2304; height = 2500; left = 48; top = 100; }) (waifu2x { noise = true; }) ]; } # danbooru 5174450
    { id = 943296; overrides = [ nsfw (crop { height = 3200; }) ]; } # danbooru 4195667
    { id = 891019; overrides = [ (crop { height = 1300; }) ]; }
    { id = 889119; overrides = [ (crop { height = 1400; }) ]; }
    { id = 817733; overrides = [ (crop { height = 1400; }) ]; }
    { id = 773046; overrides = [ (crop { height = 1400; }) ]; }
    { id = 758955; overrides = [ (crop { height = 1600; }) ]; }
    { id = 753953; overrides = [ (crop { height = 1400; }) ]; }
    { id = 744582; overrides = [ nsfw (crop { height = 1400; }) ]; }
    { id = 731377; overrides = [ nsfw ]; }
    { id = 731376; overrides = [ nsfw (crop { height = 1350; }) ]; }
    { id = 728688; overrides = [ sfw (crop { height = 2200; }) ]; }
    { id = 724231; overrides = [ (crop { height = 1350; }) ]; }
    { id = 715662; overrides = [ (crop { height = 1300; }) ]; }
    { id = 710018; overrides = [ (crop { height = 1400; }) ]; }
    { id = 707515; overrides = [ (crop { height = 1600; }) ]; }
    { id = 704246; overrides = [ nsfw (crop { height = 2000; }) ]; }
    { id = 700580; overrides = [ (crop { height = 2500; }) ]; }
    { id = 686030; overrides = [ nsfw ]; }
    680064
    { id = 662126; overrides = [ nsfw (crop { height = 2000; }) ]; }
  ];
}
