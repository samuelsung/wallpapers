# Seicoh
# - [danbooru - seicoh](https://danbooru.donmai.us/artists/53347)
# - [yande.re - seicoh](https://yande.re/wiki/show?title=seicoh)
{ config, ... }:
let
  inherit (config.helpers.overrides) sfw;
in
{
  config.moe2nix.danbooru = [
    5586046
    5522811
    4822730 # yande.re 861486
    4270104 # yande.re 653790
    4230327 # yande.re 712374
    3907370 # yande.re 635787
    3877940 # yande.re 628930
    3852520 # yande.re 624371
    3845111
    { id = 3420370; overrides = [ sfw ]; }
    { id = 3086445; overrides = [ sfw ]; }
  ];
}
