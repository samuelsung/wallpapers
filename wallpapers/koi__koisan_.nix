# Koi
# - [danbooru - koi (koisan)](https://danbooru.donmai.us/artists/16661)
# - [yande.re - koi](https://yande.re/wiki/show?title=koi)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop nsfw;
in
{
  config.moe2nix.danbooru = [
    { id = 7407805; overrides = [ nsfw (crop { height = 2500; }) ]; }
    { id = 7407804; overrides = [ (crop { height = 2500; }) ]; }
    { id = 7407803; overrides = [ (crop { height = 2500; }) ]; }
    { id = 7407802; overrides = [ (crop { height = 2500; }) ]; }
    { id = 7407801; overrides = [ (crop { height = 2500; }) ]; }
    { id = 7407800; overrides = [ (crop { height = 2500; }) ]; }
    { id = 5006953; overrides = [ (crop { width = 1510; height = 1083; left = 320; top = 56; }) ]; }
    { id = 4716835; overrides = [ (crop { width = 5080; height = 2300; left = 120; top = 850; }) ]; } # yande.re 708642
    { id = 4712263; overrides = [ (crop { width = 5300; height = 3780; left = 172; top = 108; }) ]; } # yande.re 708630
    { id = 4712261; overrides = [ (crop { width = 5404; height = 2976; left = 132; top = 512; }) ]; } # yande.re 708629
    { id = 4709801; overrides = [ nsfw (crop { width = 5300; height = 3772; left = 184; top = 112; }) ]; } # yande.re 708576
    { id = 4709787; overrides = [ (crop { width = 5248; height = 3780; left = 204; top = 112; }) ]; } # yande.re 708593
    { id = 4707464; overrides = [ (crop { width = 5252; height = 3276; left = 208; top = 92; }) ]; } # yande.re 708544
    { id = 4705573; overrides = [ (crop { width = 5404; height = 3772; left = 120; top = 112; }) ]; } # yande.re 708524
    { id = 4329621; overrides = [ (crop { width = 3877; height = 2784; left = 1720; top = 120; }) ]; } # yande.re 727486
    { id = 4327490; overrides = [ (crop { width = 4996; height = 3296; left = 124; top = 220; }) ]; } # yande.re 727388
    { id = 4327487; overrides = [ (crop { width = 5252; height = 3576; left = 200; top = 120; }) ]; } # yande.re 727381
    { id = 4327461; overrides = [ (crop { width = 5248; height = 3772; left = 196; top = 124; }) ]; } # yande.re 727349
    4269655 # yande.re 721490
    3854329 # yande.re 288130
    { id = 3560197; overrides = [ (crop { width = 4576; height = 3228; left = 112; top = 100; }) ]; } # yande.re 294674
    3558625 # yande.re 334093
    3558623 # yande.re 334094
    { id = 3558618; overrides = [ (crop { height = 3150; }) ]; } # yande.re 334143
    { id = 3558606; overrides = [ (crop { height = 2800; }) ]; } # yande.re 337598
    3558598 # yande.re 356639
    { id = 3558520; overrides = [ (crop { width = 4356; left = 320; }) ]; } # yande.re 540474
    { id = 3558519; overrides = [ (crop { width = 4081; left = 462; }) ]; } # yande.re 540475
    2623377 # yande.re 285760
    2387018 # yande.re 336584
    { id = 2378224; overrides = [ nsfw ]; } # yande.re 356753
  ];

  config.moe2nix."yande.re" = [
    1168104
    { id = 1076590; overrides = [ (crop { width = 2721; height = 1600; left = 1120; }) ]; }
    { id = 1001693; overrides = [ (crop { width = 2245; height = 1260; left = 17; top = 14; }) ]; }
  ];
}
