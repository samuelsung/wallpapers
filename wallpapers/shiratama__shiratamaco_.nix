# しらたま
# - [danbooru - shiratama (shiratamaco)](https://danbooru.donmai.us/artists/5964)
# - [yande.re - shiratama](https://yande.re/wiki/show?title=shiratama)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop rotate nsfw sfw;
in
{
  config.moe2nix.danbooru = [
    { id = 8045910; overrides = [ (rotate 270) ]; }
    7081055
    7080907
    { id = 6671828; overrides = [ nsfw ]; }
    6448881
    6273846
    5760100 # yande.re 1018658
    { id = 5725357; overrides = [ (crop { height = 800; }) ]; }
    5531135 # yande.re 1002433
    { id = 5259034; overrides = [ sfw ]; }
    5044458
    4878444
    4715388
    4690650 # danbooru 4690651
    { id = 4625469; overrides = [ sfw ]; } # yande.re 812447
    { id = 4374727; overrides = [ sfw ]; }
    4230350 # yande.re 730067
    4208660
    { id = 4165987; overrides = [ sfw ]; }
    { id = 3979741; overrides = [ (crop { height = 3000; }) ]; } # yande.re 643048
    { id = 3970577; overrides = [ (rotate 90) ]; } # or yande.re 793901
    3781942
    3570449
    { id = 3561851; overrides = [ (crop { width = 3234; }) ]; } # yande.re 482820
    { id = 3561060; overrides = [ (crop { width = 3276; }) ]; } # yande.re 482821
    3484154 # yande.re 530922
    3400543 # yande.re 513072
    { id = 3036570; overrides = [ sfw (crop { height = 1800; }) ]; } # yande.re 432578
    { id = 3030744; overrides = [ sfw ]; } # yande.re 432037
    { id = 3013237; overrides = [ (crop { height = 1800; }) ]; } # yande.re 432545
    { id = 2515838; overrides = [ (crop { height = 3000; }) ]; } # yande.re 371799
    { id = 2504100; overrides = [ (crop { height = 3000; }) ]; } # yande.re 370216
    { id = 2362106; overrides = [ sfw (crop { width = 3948; height = 2000; }) ]; } # yande.re 331709
    2215095 # yande.re 340974
    2170169
  ];

  config.moe2nix."yande.re" = [
    { id = 1200386; overrides = [ (crop { width = 3658; height = 3096; left = 1240; top = 160; }) ]; }
    { id = 1200381; overrides = [ nsfw (crop { height = 3000; }) ]; }
    { id = 1200377; overrides = [ (crop { height = 3000; }) ]; } # danbooru 6288458
    { id = 1200373; overrides = [ (crop { height = 3000; }) ]; }
    { id = 1200370; overrides = [ (crop { height = 2576; }) ]; }
    1064164
    { id = 1047933; overrides = [ nsfw ]; } # danbooru 5883565
    { id = 1018656; overrides = [ (crop { width = 4626; height = 3000; }) ]; } # danbooru 4813882
    1018645 # danbooru 5510792
    { id = 999063; overrides = [ nsfw (crop { height = 1100; }) ]; } # danbooru 4370033
    { id = 968495; overrides = [ (crop { width = 3570; height = 2544; left = 129; top = 321; }) ]; } # danbooru 4486919
    { id = 893880; overrides = [ nsfw (crop { height = 1300; }) ]; } # danbooru 363891r
    887784
    845544
    845489
    845481 # danbooru 4069723
    845480
    702312 # danbooru 3972029
    643054 # danbooru 3716156
    643049 # danbooru 3875150
    577537
    { id = 570940; overrides = [ (crop { width = 3947; left = 932; }) ]; }
    { id = 570938; overrides = [ (crop { width = 3234; }) ]; } # danbooru 3358591
    570936 # danbooru 3359918
    { id = 482816; overrides = [ nsfw ]; }
    432632
    { id = 432631; overrides = [ sfw ]; }
    { id = 432610; overrides = [ sfw (crop { width = 2506; height = 1768; top = 50; }) ]; } # danbooru 2362116
    { id = 431122; overrides = [ (crop { height = 1300; }) ]; }
    { id = 411223; overrides = [ (crop { height = 3700; }) ]; }
    { id = 411222; overrides = [ (crop { height = 3500; }) ]; } # danbooru 2845204
    { id = 432607; overrides = [ (crop { width = 2564; height = 1818; left = 146; }) ]; }
    385404
  ];
}
