# nen
# - [danbooru - hanen_(borry)](https://danbooru.donmai.us/artists/134331)
# - [yande.re - hanen_(borry)](https://yande.re/wiki/show?title=hanen_%28borry%29)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop sfw;
in
{
  config.moe2nix.danbooru = [
    { id = 8436074; overrides = [ (crop { height = 1200; }) ]; }
    { id = 7772137; overrides = [ sfw (crop { height = 1000; }) ]; }
    { id = 6706471; overrides = [ (crop { height = 2650; }) ]; }
    6687859
    5936170
    { id = 5850097; overrides = [ sfw (crop { height = 3000; }) ]; } # yande.re 1040725
    { id = 5635493; overrides = [ (crop { height = 2000; }) ]; }
    { id = 5635371; overrides = [ sfw ]; }
    { id = 5254648; overrides = [ (crop { height = 2100; }) ]; } # yande.re 950722
    { id = 5168930; overrides = [ sfw (crop { height = 3500; }) ]; } # yande.re 936986
    { id = 5020180; overrides = [ (crop { height = 2850; }) ]; }
    { id = 5007154; overrides = [ sfw (crop { height = 2850; }) ]; } # yande.re 903967
    4876091
    4764613 # yande.re 848286
    { id = 4683380; overrides = [ (crop { height = 2950; }) ]; }
    { id = 4671066; overrides = [ (crop { height = 3000; }) ]; }
    { id = 4589570; overrides = [ sfw (crop { height = 2500; }) ]; } # yande.re 803508
    { id = 4515843; overrides = [ sfw (crop { height = 2900; }) ]; } # yande.re 783724
    { id = 4358299; overrides = [ (crop { height = 2950; }) ]; }
  ];
}
