# 桜
# [danbooru - sakura_ran](https://safebooru.donmai.us/artists/50569)
# [yande.re - sakura_ran](https://yande.re/wiki/show?title=sakura_ran)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop sfw rotate;
in
{
  config.moe2nix.danbooru = [
    { id = 8606161; overrides = [ (crop { height = 900; }) ]; }
    { id = 8572791; overrides = [ (crop { height = 900; }) ]; }
    { id = 8486857; overrides = [ sfw (crop { height = 1300; }) ]; }
    { id = 8483072; overrides = [ (crop { height = 1000; }) ]; }
    { id = 8437835; overrides = [ (crop { height = 1000; }) ]; }
    { id = 8329139; overrides = [ (crop { height = 975; }) ]; }
    7613437
    { id = 7511416; overrides = [ (crop { height = 3000; }) ]; }
    { id = 7452014; overrides = [ (crop { height = 1000; }) ]; }
    { id = 7067389; overrides = [ (crop { height = 1000; }) ]; }
    6902362
    6830712
    { id = 6706410; overrides = [ (rotate 90) ]; }
    6649274
    { id = 6414209; overrides = [ (crop { height = 2500; }) ]; }
    { id = 6401184; overrides = [ (crop { height = 1264; top = 200; }) ]; }
    { id = 6366365; overrides = [ sfw (crop { height = 1500; }) ]; }
    { id = 6286967; overrides = [ (crop { height = 850; }) ]; }
    { id = 6169502; overrides = [ (rotate 270) ]; }
    { id = 6165086; overrides = [ (crop { height = 900; }) ]; }
    { id = 5942720; overrides = [ (crop { height = 1000; }) ]; }
    { id = 5885309; overrides = [ (crop { height = 1200; }) ]; }
    { id = 5869867; overrides = [ sfw (crop { height = 750; }) ]; }
    { id = 5738722; overrides = [ (crop { height = 1500; }) ]; }
    { id = 5711970; overrides = [ sfw ]; }
    { id = 5562658; overrides = [ (crop { height = 2000; }) ]; }
    5481526
    { id = 5341207; overrides = [ (crop { height = 2800; }) ]; }
    { id = 5341205; overrides = [ (crop { height = 2000; }) ]; }
    { id = 5331917; overrides = [ (crop { height = 2000; }) ]; }
    { id = 5256407; overrides = [ (crop { height = 1800; }) ]; }
    { id = 5256401; overrides = [ (crop { height = 1200; }) ]; }
    { id = 5233955; overrides = [ (crop { height = 1600; }) ]; }
    5167184
    5019801
    4245420
    { id = 4222610; overrides = [ sfw (crop { height = 3000; }) ]; }
    { id = 4208573; overrides = [ sfw (crop { height = 1800; }) ]; } # yande.re 707554
    { id = 3909360; overrides = [ sfw ]; }
  ];
}
