# 池上茜
# - [danbooru - ikegami_akane](https://danbooru.donmai.us/artists/8165)
# - [yande.re - ikegami_akane](https://yande.re/wiki/show?title=ikegami_akane)
{ config, ... }:
let
  inherit (config.helpers.overrides) sfw nsfw crop;
in
{
  config.moe2nix.danbooru = [
    { id = 7950080; overrides = [ sfw (crop { height = 700; }) ]; }
    { id = 7795231; overrides = [ (crop { height = 700; }) ]; }
    { id = 7639139; overrides = [ sfw (crop { height = 700; }) ]; }
    { id = 7196187; overrides = [ (crop { height = 800; }) ]; }
    { id = 7056727; overrides = [ (crop { height = 700; }) ]; }
    { id = 6641041; overrides = [ (crop { height = 800; }) ]; }
    4190820 # yande.re 701577
    3875744
    { id = 3875736; overrides = [ sfw ]; }
    { id = 3590123; overrides = [ sfw ]; }
    { id = 3105055; overrides = [ sfw ]; } # yande.re 442031
    2695040 # yande.re 389732
    2693565 # yande.re 389730
    { id = 2653336; overrides = [ sfw ]; }
    2652110
    2652108 # yande.re 385817
    2651316 # yande.re 385740
    2458143 # yande.re 275286
    2173817
    2172878 # yande.re 274385
    { id = 1888903; overrides = [ (crop { height = 1137; top = 39; }) ]; } # yande.re 307909
    1851436 # yande.re 274383
    { id = 1595134; overrides = [ (crop { height = 926; top = 37; }) ]; }
    1531999 # yande.re 270869
    { id = 1414182; overrides = [ (crop { height = 1064; top = 36; }) ]; } # yande.re 245840
    { id = 1179187; overrides = [ (crop { height = 900; top = 39; }) ]; } # yande.re 189996
    { id = 1065123; overrides = [ sfw (crop { width = 2304; height = 1600; left = 17; top = 49; }) ]; } # yande.re 155152
    940504 # yande.re 210601
  ];

  config.moe2nix."yande.re" = [
    { id = 1106526; overrides = [ (crop { width = 1740; height = 881; top = 113; }) ]; }
    { id = 1082847; overrides = [ sfw (crop { height = 2500; }) ]; }
    { id = 908661; overrides = [ sfw (crop { height = 1100; }) ]; }
    { id = 908654; overrides = [ sfw (crop { width = 2480; height = 1770; left = 100; }) ]; }
    { id = 908647; overrides = [ sfw (crop { width = 2564; height = 1818; }) ]; }
    { id = 908634; overrides = [ (crop { width = 1124; height = 1300; left = 134; top = 102; }) ]; }
    { id = 776645; overrides = [ nsfw (crop { width = 7588; left = 892; }) ]; }
    701667
    { id = 701639; overrides = [ (crop { height = 2000; }) ]; }
    { id = 701638; overrides = [ (crop { height = 2000; }) ]; }
    { id = 701636; overrides = [ nsfw ]; }
    { id = 701634; overrides = [ nsfw ]; }
    { id = 701633; overrides = [ nsfw ]; }
    { id = 701632; overrides = [ nsfw ]; }
    { id = 701630; overrides = [ nsfw ]; }
    { id = 701628; overrides = [ nsfw ]; }
    701626
    { id = 701625; overrides = [ (crop { height = 1700; }) ]; }
    701622
    { id = 701618; overrides = [ (crop { height = 1600; }) ]; }
    { id = 701617; overrides = [ (crop { height = 1700; }) ]; }
    { id = 701616; overrides = [ (crop { height = 2000; }) ]; }
    { id = 701615; overrides = [ (crop { height = 2200; }) ]; }
    { id = 701599; overrides = [ (crop { height = 1800; }) ]; }
    { id = 701585; overrides = [ (crop { height = 1800; }) ]; }
    { id = 701559; overrides = [ (crop { height = 2000; }) ]; }
    701532
    { id = 701531; overrides = [ (crop { height = 1700; }) ]; }
    { id = 701530; overrides = [ (crop { height = 1800; }) ]; }
    701527
    { id = 701526; overrides = [ (crop { height = 1900; }) ]; } # danbooru 3271622
    { id = 701525; overrides = [ (crop { height = 1800; }) ]; }
    685273
    { id = 665860; overrides = [ sfw (crop { height = 1200; }) ]; }
    { id = 649132; overrides = [ (crop { height = 900; }) ]; }
    { id = 384798; overrides = [ (crop { height = 900; }) ]; }
    { id = 379679; overrides = [ (crop { height = 937; top = 84; }) ]; }
    338935
    338933
    { id = 322944; overrides = [ nsfw ]; }
    322942
    322941
    { id = 297498; overrides = [ (crop { height = 1142; top = 36; }) ]; }
    { id = 297497; overrides = [ (crop { height = 1140; top = 36; }) ]; }
    { id = 297492; overrides = [ (crop { height = 1140; top = 36; }) ]; }
    { id = 297490; overrides = [ (crop { height = 1143; top = 36; }) ]; }
    { id = 297489; overrides = [ (crop { height = 1142; top = 34; }) ]; }
    { id = 297398; overrides = [ (crop { height = 1141; top = 37; }) ]; }
    275285
    274386
    274384
    274176
    { id = 255440; overrides = [ (crop { height = 2800; }) ]; } # danbooru 1419865
    { id = 208473; overrides = [ (crop { height = 999; top = 38; }) ]; }
    { id = 203350; overrides = [ (crop { height = 1033; top = 39; }) ]; }
    { id = 194646; overrides = [ (crop { height = 999; top = 38; }) ]; }
    189999
    { id = 189997; overrides = [ (crop { height = 999; top = 38; }) ]; }
    { id = 189995; overrides = [ (crop { height = 1033; top = 39; }) ]; }
  ];
}
