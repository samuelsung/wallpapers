# 飴玉コン
# - [danbooru - amedamacon](https://danbooru.donmai.us/artists/118913)
# - [yande.re - amedama_con](https://yande.re/wiki/show?title=amedama_con)
{ config, ... }:
let
  inherit (config.helpers.overrides) sfw nsfw crop waifu2x rotate;
in
{
  config.moe2nix.danbooru = [
    8073450
    { id = 7971271; overrides = [ (crop { height = 1500; }) ]; } # yande.re 1187911
    { id = 7945971; overrides = [ (crop { height = 1500; }) ]; } # yande.re 1187925
    7944327
    { id = 7626579; overrides = [ (crop { height = 1150; }) ]; }
    { id = 7503610; overrides = [ (crop { height = 1050; }) ]; }
    { id = 7248820; overrides = [ (crop { height = 950; }) ]; } # yande.re 1185845
    7029952
    { id = 6845720; overrides = [ nsfw (crop { height = 1450; }) ]; }
    { id = 6766319; overrides = [ (crop { height = 1100; }) ]; } # yande.re 1125948
    5568140
    { id = 5566427; overrides = [ sfw (crop { height = 1000; }) ]; }
    { id = 5204641; overrides = [ (crop { width = 3320; height = 2200; }) (waifu2x { noise = true; }) ]; } # yande.re 658321
    { id = 5086520; overrides = [ (crop { height = 2700; }) ]; } # yande.re 1185868
    { id = 4970868; overrides = [ sfw (crop { height = 1000; }) ]; }
    { id = 4908229; overrides = [ sfw (crop { height = 1700; }) ]; } # yande.re 896398
    { id = 4907218; overrides = [ sfw (crop { height = 900; }) ]; }
    { id = 4740768; overrides = [ (crop { height = 1300; }) ]; } # yande.re 896397
    { id = 4589628; overrides = [ (crop { height = 1100; top = 100; }) ]; }
    { id = 4510141; overrides = [ (crop { height = 1000; }) ]; }
    { id = 4474664; overrides = [ (crop { height = 950; }) ]; }
    { id = 4376643; overrides = [ sfw (crop { height = 900; }) ]; }
    { id = 4354005; overrides = [ (crop { width = 2588; height = 2500; left = 168; top = 128; }) (waifu2x { noise = true; }) ]; } # yande.re 742293
    { id = 4274861; overrides = [ (crop { height = 1000; }) ]; } # yande.re 738969
    { id = 3922549; overrides = [ (crop { height = 1400; }) ]; } # yande.re 691642
    { id = 3887748; overrides = [ sfw (crop { height = 1500; }) ]; } # yande.re 630885
    { id = 3885598; overrides = [ (crop { height = 1000; }) ]; } # yande.re 630350
    { id = 3846989; overrides = [ (crop { height = 1300; top = 260; }) ]; } # yande.re 658313
    { id = 3828143; overrides = [ sfw (crop { height = 1400; }) (waifu2x { noise = true; }) ]; } # yande.re 658312
    3798410 # yande.re 613180
    { id = 3788278; overrides = [ (crop { height = 1700; }) ]; } # yande.re 658323
    { id = 3725893; overrides = [ (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # yande.re 597006
    3723584 # yande.re 596447
    3721223 # yande.re 595766
    { id = 3716669; overrides = [ sfw (crop { height = 1600; }) ]; } # yande.re 594748
    { id = 3706368; overrides = [ (crop { height = 1850; }) (waifu2x { noise = true; }) (waifu2x { noise = true; }) (waifu2x { noise = true; }) ]; } # yande.re 592885
    { id = 3706339; overrides = [ (crop { height = 1600; }) ]; } # yande.re 592886
    { id = 3646721; overrides = [ (crop { height = 1300; top = 400; }) ]; } # yande.re 644864
    { id = 3645997; overrides = [ (crop { height = 1550; }) ]; } # yande.re 644863
    { id = 3644219; overrides = [ (crop { height = 1200; }) ]; } # yande.re 576273
    3643247
    { id = 3631432; overrides = [ (rotate 180) ]; }
    { id = 3596141; overrides = [ (crop { height = 3000; }) (waifu2x { noise = true; }) ]; }
    3490482 # yande.re 600827
    3365124
    3280904
    3219307 # yande.re 472525
    { id = 3211900; overrides = [ (crop { height = 1200; }) ]; } # yande.re 471218
  ];

  config.moe2nix."yande.re" = [
    { id = 1185894; overrides = [ (waifu2x { noise = true; }) ]; }
    { id = 1185896; overrides = [ nsfw (crop { height = 2250; }) (waifu2x { noise = true; }) ]; } # danbooru 6263616
    { id = 1185895; overrides = [ nsfw (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # danbooru 5931419
    { id = 1185885; overrides = [ nsfw (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # danbooru 6404675
    { id = 1185866; overrides = [ nsfw (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # danbooru 5129515
    { id = 1185852; overrides = [ (crop { width = 2823; height = 2000; }) (waifu2x { noise = true; }) ]; } # danbooru 7010183
    { id = 1185851; overrides = [ (crop { height = 2100; }) (waifu2x { noise = true; }) ]; } # danbooru 8150665
    { id = 1185848; overrides = [ (crop { height = 2250; }) (waifu2x { noise = true; }) ]; } # danbooru 7282101
    { id = 1185846; overrides = [ (waifu2x { noise = true; }) ]; } # danbooru 7248823
    { id = 1135528; overrides = [ (crop { height = 2550; }) (waifu2x { noise = true; }) ]; }
    { id = 1135526; overrides = [ (crop { height = 2000; top = 340; }) (waifu2x { noise = true; }) ]; } # danbooru 5424253
    { id = 1135523; overrides = [ nsfw (crop { width = 1912; height = 2000; left = 60; top = 30; }) (waifu2x { noise = true; }) ]; } # danbooru 6554574
    { id = 1135522; overrides = [ nsfw (crop { height = 1900; }) (waifu2x { noise = true; }) ]; } # danbooru 6479687
    { id = 1135521; overrides = [ (crop { height = 2100; }) (waifu2x { noise = true; }) ]; } # danbooru 6535424
    { id = 1135520; overrides = [ (crop { width = 2020; height = 2250; left = 70; top = 30; }) (waifu2x { noise = true; }) ]; } # danbooru 6330731
    { id = 1088924; overrides = [ (crop { height = 2400; }) (waifu2x { noise = true; }) ]; } # danbooru 5650966
    { id = 1088913; overrides = [ (crop { height = 2100; }) (waifu2x { noise = true; }) ]; } # danbooru 6114434
    { id = 1041977; overrides = [ (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # danbooru 5481941
    { id = 1041971; overrides = [ nsfw (crop { height = 1900; }) (waifu2x { noise = true; }) ]; } # danbooru 5578508
    { id = 1041969; overrides = [ sfw (crop { height = 1700; }) (waifu2x { noise = true; }) ]; } # danbooru 5494499
    { id = 968315; overrides = [ (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # danbooru 5419445
    { id = 902291; overrides = [ (crop { height = 1100; }) ]; }
    { id = 896404; overrides = [ nsfw (crop { height = 1900; }) (waifu2x { noise = true; }) ]; } # danbooru 4833120
    { id = 896402; overrides = [ (crop { height = 1700; }) (waifu2x { noise = true; }) ]; } # danbooru 5009270
    { id = 896400; overrides = [ (crop { height = 2800; }) (waifu2x { noise = true; }) ]; }
    { id = 691653; overrides = [ (crop { width = 1983; height = 1600; left = 60; top = 75; }) (waifu2x { noise = true; }) ]; } # danbooru 4119325
    { id = 691651; overrides = [ (crop { width = 1983; height = 2250; left = 36; top = 48; }) (waifu2x { noise = true; }) ]; } # danbooru 3752134
    { id = 691647; overrides = [ (crop { width = 1977; height = 2000; left = 57; top = 69; }) (waifu2x { noise = true; }) ]; } # danbooru 4119189
    { id = 691646; overrides = [ (crop { width = 1992; height = 1600; left = 21; top = 48; }) (waifu2x { noise = true; }) ]; } # danbooru 4119328
    { id = 691645; overrides = [ nsfw (crop { width = 1983; height = 1800; left = 60; top = 78; }) (waifu2x { noise = true; }) ]; } # danbooru 4096730
    { id = 665106; overrides = [ (crop { height = 2100; }) (waifu2x { noise = true; }) ]; }
    { id = 658320; overrides = [ (crop { height = 2600; }) (waifu2x { noise = true; }) ]; } # danbooru 3751655
    { id = 630890; overrides = [ (crop { height = 987; top = 400; }) (waifu2x { noise = true; }) ]; } # danbooru 3887751
    { id = 600821; overrides = [ nsfw (crop { width = 2959; height = 3800; left = 116; top = 254; }) (waifu2x { noise = true; }) ]; } # danboru 3472153
    { id = 600820; overrides = [ (crop { height = 2800; }) (waifu2x { noise = true; }) ]; } # danbooru 3438770
    { id = 593520; overrides = [ nsfw (crop { height = 4500; }) (waifu2x { noise = true; }) ]; } # danbooru 3537010
    { id = 593516; overrides = [ (crop { height = 4000; }) (waifu2x { noise = true; }) ]; } # danbooru 3589157
    { id = 593513; overrides = [ (crop { height = 4500; }) (waifu2x { noise = true; }) ]; } # danbooru 3588065
    { id = 593512; overrides = [ (crop { height = 5000; }) (waifu2x { noise = true; }) ]; }
    { id = 496970; overrides = [ (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # danbooru 3149386
    { id = 496969; overrides = [ nsfw (crop { height = 2500; }) (waifu2x { noise = true; }) ]; }
  ];
}
