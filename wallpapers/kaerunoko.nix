# かえるのこ
# - [danbooru - kaerunoko](https://danbooru.donmai.us/artists/143194)
# - [yande.re - kaerunoko](https://yande.re/wiki/show?title=kaerunoko)
{ config, ... }:
let
  inherit (config.helpers.overrides) sfw crop rotate;
in
{
  config.moe2nix.danbooru = [
    7221808
    { id = 7221806; overrides = [ sfw (crop { height = 1400; }) ]; } # yande.re 1152939
    7221804 # yande.re 1152937
    6527093 # yande.re 1152941
    { id = 4624156; overrides = [ (crop { width = 1238; height = 1762; top = 82; left = 78; }) (rotate 270) ]; } # yande.re 811182
    3870701 # yande.re 627671
    { id = 3655782; overrides = [ sfw (crop { height = 1100; }) ]; } # yande.re 579055
    { id = 3280359; overrides = [ (crop { height = 1400; }) ]; } # yande.re 487610
    { id = 3242515; overrides = [ (crop { height = 1000; }) ]; } # yande.re 493279
    { id = 2989011; overrides = [ (crop { height = 1200; }) ]; }
    { id = 2824991; overrides = [ (crop { height = 1200; }) ]; } # yande.re 405447
    { id = 2766369; overrides = [ (crop { height = 1500; }) ]; } # yande.re 398088
  ];

  config.moe2nix."yande.re" = [
    { id = 669733; overrides = [ (crop { height = 1400; }) ]; }
  ];
}
