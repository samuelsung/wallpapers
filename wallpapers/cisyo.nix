# ししょー
# - [danbooru - cisyo](https://danbooru.donmai.us/artists/132376)
# - [yande.re - cisyo](https://yande.re/wiki/show?title=cisyo)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop rotate;
in
{
  config.moe2nix.danbooru = [
    { id = 8093468; overrides = [ (crop { height = 1400; }) ]; } # yande.re 986960
    { id = 5850030; overrides = [ (crop { height = 1000; }) ]; }
    { id = 5592391; overrides = [ (rotate 270) ]; }
    4853049
    { id = 4811211; overrides = [ (crop { height = 900; }) ]; }
    { id = 4684320; overrides = [ (crop { height = 800; }) ]; }
    { id = 4268826; overrides = [ (crop { height = 900; }) ]; } # yande.re 718599
    4099529
    3897171
    { id = 3864279; overrides = [ (crop { height = 1000; }) ]; }
    { id = 3709049; overrides = [ (crop { height = 900; }) ]; }
    3635396
    { id = 3575011; overrides = [ (crop { height = 800; }) ]; }
    3509024
    { id = 3453978; overrides = [ (crop { height = 900; }) ]; }
    { id = 3274330; overrides = [ (crop { height = 800; }) ]; }
    3253263
    { id = 3168798; overrides = [ (crop { height = 800; }) ]; }
    { id = 2958879; overrides = [ (crop { height = 1500; }) ]; } # yande.re 503129
  ];

  config.moe2nix."yande.re" = [
    { id = 860256; overrides = [ (crop { width = 1107; height = 1250; left = 45; top = 47; }) ]; }
    { id = 781409; overrides = [ (crop { height = 1400; }) ]; } # danbooru 3509036
    { id = 781403; overrides = [ (crop { height = 1600; }) ]; } # danbooru 3372148
    { id = 771386; overrides = [ (crop { height = 2280; }) ]; } # danbooru 3858021
    { id = 750218; overrides = [ (crop { height = 2530; }) ]; } # danbooru 4369580
    { id = 735911; overrides = [ (crop { height = 2800; }) ]; } # danbooru 4852084
    { id = 735910; overrides = [ (crop { height = 2800; }) ]; }
    { id = 696355; overrides = [ (crop { height = 3100; }) ]; } # danbooru 4120694
    696334 # danbooru 4156330
    { id = 674264; overrides = [ (crop { height = 3300; }) ]; } # danbooru 4056197
    { id = 674263; overrides = [ (crop { height = 3000; }) ]; }
    643690 # danbooru 3809493
    643618 # danbooru 3936618
    { id = 640784; overrides = [ (crop { height = 2920; }) ]; } # danbooru 3904666
    { id = 615753; overrides = [ (crop { height = 3000; }) ]; }
    { id = 615752; overrides = [ (crop { height = 1600; }) ]; }
    { id = 615748; overrides = [ (crop { height = 3410; }) ]; } # danbooru 4157960
    { id = 615747; overrides = [ (crop { height = 2730; }) ]; } # danbooru 3782840
    { id = 516066; overrides = [ (crop { height = 2500; }) ]; }
    418285 # danbooru 2919319
  ];
}
