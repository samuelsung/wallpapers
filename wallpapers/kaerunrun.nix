# カエルンルン
# - [danbooru - kaerunrun](https://danbooru.donmai.us/artists/263125)
# - [yande.re - kaerunoko](https://yande.re/wiki/show?title=kaerunrun)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop nsfw sfw waifu2x;
in
{
  config.moe2nix.danbooru = [
    { id = 8668235; overrides = [ sfw (waifu2x { noise = true; }) ]; }
    8611649
    8237010
    7984464
    7928450
    { id = 7887498; overrides = [ (crop { height = 850; }) ]; }
    7521158
    7521157
    { id = 7426438; overrides = [ (crop { height = 1200; }) ]; }
    { id = 7395244; overrides = [ sfw (crop { height = 1200; }) ]; }
    { id = 7395221; overrides = [ (waifu2x { noise = false; }) ]; }
    7227784
    7195519
    7075322
    7050390
    7044024
    { id = 7013312; overrides = [ nsfw ]; }
    { id = 6381131; overrides = [ sfw ]; }
  ];

  config.moe2nix."konachan.com" = [
    { id = 375487; overrides = [ ]; } # danbooru 7426442
  ];

  config.moe2nix."yande.re" = [
    { id = 1106529; overrides = [ nsfw ]; } # danbooru 7734028
  ];
}
