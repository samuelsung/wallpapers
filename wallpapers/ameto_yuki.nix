# あめとゆき
# - [danbooru - ameto_yuki](https://danbooru.donmai.us/artists/17822)
# - [yande.re - ameto_yuki](https://yande.re/wiki/show?title=ameto_yuki)
{ config, ... }:
let
  inherit (config.helpers.overrides) sfw nsfw crop rotate waifu2x;
in
{
  config.moe2nix.danbooru = [
    { id = 5924075; overrides = [ (crop { height = 1000; }) ]; } # yande.re 906465
    { id = 5888577; overrides = [ (crop { height = 1000; }) ]; }
    { id = 5584187; overrides = [ sfw ]; }
    5478266
    { id = 5168981; overrides = [ (crop { height = 1400; }) ]; } # yande.re 936950
    { id = 5021693; overrides = [ sfw ]; } # yande.re 906517
    4525706
    { id = 4274464; overrides = [ sfw ]; } # yande.re 721361
    { id = 3982735; overrides = [ (crop { height = 1400; }) ]; } # yande.re 655902 or danbooru 3930307
    { id = 3868399; overrides = [ sfw ]; }
    { id = 3787915; overrides = [ sfw (crop { height = 1200; }) ]; } # yande.re 608060
    { id = 3787914; overrides = [ (crop { height = 1300; }) ]; } # yande.re 608061
    { id = 3787913; overrides = [ (crop { height = 1400; }) ]; } # yande.re 608058
    { id = 3787912; overrides = [ (crop { height = 1400; }) ]; } # yande.re 608057
    { id = 3787911; overrides = [ (crop { height = 1400; }) ]; } # yande.re 608056 or yande.re 1135009
    { id = 3787910; overrides = [ (crop { height = 1400; }) ]; } # yande.re 608055
    { id = 3721342; overrides = [ (crop { height = 1800; }) ]; } # yande.re 595800
    3705414 # yande.re 592750
    { id = 3646500; overrides = [ (crop { height = 1200; }) ]; } # or yande.re 577858
    { id = 3640558; overrides = [ (crop { height = 1000; }) ]; } # yande.re 573942
    3626913
    3484014
    3472107 # yande.re 531825
    3435894
    3351857 # yande.re 507118
    { id = 3341338; overrides = [ sfw (crop { height = 1000; }) ]; }
    3285013 # yande.re 488568
    { id = 3282651; overrides = [ sfw (crop { height = 2200; }) ]; } # yande.re 484055
    { id = 3200448; overrides = [ sfw ]; }
    { id = 2992138; overrides = [ (crop { height = 2500; }) ]; } # yande.re 430308
    { id = 2802632; overrides = [ sfw (crop { height = 1000; }) ]; } # or yande.re 408193
    { id = 2771192; overrides = [ (crop { height = 1400; }) ]; } # yande.re 398854
    { id = 2754971; overrides = [ (crop { height = 1100; }) ]; }
    2704281 # yande.re 390874
    { id = 2672000; overrides = [ sfw (crop { height = 1000; }) ]; }
    2642316
    { id = 2629364; overrides = [ (crop { height = 2500; }) ]; } # yande.re 383628
    2516701 # yande.re 371549
    { id = 2508792; overrides = [ sfw ]; } # yande.re 371121
    2508789 # yande.re 371120
    2504095 # yande.re 370469
    2446181
    2220411
    2220343
    2157464
  ];

  config.moe2nix."yande.re" = [
    { id = 1208377; overrides = [ (crop { height = 3000; }) ]; } # danbooru 5929721
    { id = 1208373; overrides = [ (crop { height = 3000; }) ]; }
    { id = 1208372; overrides = [ (crop { height = 3500; }) ]; }
    1208035
    { id = 1153597; overrides = [ (crop { height = 1300; }) ]; }
    { id = 1105737; overrides = [ (crop { height = 3000; }) ]; } # danbooru 3787916
    { id = 1105735; overrides = [ (crop { height = 3000; }) ]; }
    { id = 1105733; overrides = [ (crop { height = 3500; }) ]; }
    { id = 1105729; overrides = [ (crop { height = 3000; }) ]; } # danbooru 5236411
    { id = 1087360; overrides = [ (crop { height = 1200; }) ]; } # danbooru 6267791
    { id = 1058449; overrides = [ (crop { height = 1200; }) ]; }
    { id = 1058448; overrides = [ (crop { height = 1500; }) ]; }
    { id = 1058447; overrides = [ (crop { height = 1700; }) ]; }
    876153
    { id = 860241; overrides = [ (crop { width = 1107; height = 1102; left = 45; top = 47; }) ]; } # danbooru 6321473
    { id = 811738; overrides = [ (crop { height = 1300; }) ]; } # or danbooru 4621147
    { id = 757717; overrides = [ (crop { width = 2992; height = 3328; left = 226; top = 138; }) (waifu2x { noise = true; }) ]; } # danbooru 4017364
    753156
    { id = 695630; overrides = [ (crop { width = 7864; height = 5656; left = 176; top = 152; }) ]; } # danbooru 4154904
    { id = 692372; overrides = [ (crop { height = 1300; }) ]; }
    { id = 685402; overrides = [ (crop { height = 1300; }) ]; }
    { id = 614927; overrides = [ (crop { height = 1300; }) ]; }
    { id = 608125; overrides = [ (crop { height = 1500; }) ]; }
    { id = 608124; overrides = [ (rotate 270) (crop { height = 1500; }) ]; }
    608120
    { id = 608119; overrides = [ (crop { height = 1500; }) ]; }
    { id = 608109; overrides = [ (crop { width = 1160; height = 1160; left = 72; top = 380; }) ]; }
    { id = 608107; overrides = [ sfw (crop { height = 1300; }) ]; } # danbooru 3118372
    { id = 608103; overrides = [ (crop { width = 2606; height = 1736; top = 60; }) ]; }
    608102
    608101
    { id = 608098; overrides = [ (crop { height = 1500; }) ]; } # danbooru 1866267
    { id = 608092; overrides = [ sfw (crop { height = 1300; }) ]; }
    608090
    { id = 608089; overrides = [ (crop { height = 1400; }) ]; } # danbooru 2327777
    { id = 608087; overrides = [ (crop { height = 1300; }) ]; }
    608086 # danbooru 2638455
    { id = 608084; overrides = [ (crop { height = 1500; }) ]; } # danbooru 2886131
    { id = 608082; overrides = [ sfw (crop { height = 1500; }) ]; } # danbooru 3039883
    { id = 608081; overrides = [ (crop { width = 1190; height = 1100; left = 57; top = 182; }) ]; }
    { id = 608080; overrides = [ (crop { height = 1400; }) ]; }
    { id = 608079; overrides = [ (rotate 270) (crop { width = 1477; height = 1196; left = 221; top = 106; }) ]; } # danbooru 3300608
    { id = 608078; overrides = [ (crop { height = 1500; }) ]; } # danbooru 3325656
    608077 # danbooru 3359661
    { id = 608076; overrides = [ (crop { height = 1400; }) ]; } # danbooru 8351690
    { id = 608075; overrides = [ (crop { height = 1400; }) ]; } # danbooru 3303260 3295165
    { id = 608074; overrides = [ (crop { height = 1400; }) ]; }
    { id = 608073; overrides = [ (crop { height = 1400; }) ]; }
    { id = 608071; overrides = [ (crop { height = 1400; }) ]; }
    { id = 608070; overrides = [ (crop { height = 1200; }) ]; }
    { id = 608068; overrides = [ (crop { height = 1400; }) ]; }
    { id = 608066; overrides = [ (crop { height = 1200; }) ]; } # danbooru 2412645
    { id = 608059; overrides = [ (crop { height = 1450; }) ]; }
    { id = 608049; overrides = [ (crop { height = 1400; }) ]; }
    { id = 608045; overrides = [ (crop { height = 1400; }) ]; } # danbooru 3333675
    { id = 608043; overrides = [ (crop { height = 1300; }) ]; }
    { id = 608041; overrides = [ (crop { height = 1300; }) ]; }
    { id = 608036; overrides = [ (crop { height = 1300; }) ]; } # danbooru 3761010
    { id = 608034; overrides = [ (crop { height = 1300; }) ]; } # danbooru 3655100
    { id = 608032; overrides = [ (crop { height = 1300; }) ]; } # danbooru 2671179
    { id = 608030; overrides = [ (crop { height = 1300; }) ]; }
    { id = 608029; overrides = [ (crop { height = 1300; }) ]; }
    { id = 608028; overrides = [ sfw (crop { height = 1300; }) ]; }
    { id = 608027; overrides = [ sfw (crop { height = 1300; }) ]; }
    { id = 608026; overrides = [ nsfw ]; }
    { id = 608025; overrides = [ sfw (crop { height = 1300; }) ]; }
    { id = 608024; overrides = [ (crop { height = 1300; }) ]; } # danbooru 3585364
    608023 # danbooru 3680474
    { id = 608022; overrides = [ (crop { height = 1300; }) ]; }
    { id = 602822; overrides = [ (crop { height = 1400; }) ]; }
    598187 # danbooru 2754981
    { id = 598183; overrides = [ (crop { height = 1500; }) ]; } # danbooru 2939635 or yande.re 608083
    { id = 582690; overrides = [ (crop { height = 1200; }) ]; } # or danbooru 3388175
    { id = 500004; overrides = [ (crop { height = 1400; }) ]; } # danbooru 3184018
    493805 # danbooru 3063680
    468795
    370468
  ];
}
