# たかやKi
# - [danbooru - takayaki](https://danbooru.donmai.us/artists/4851)
# - [yande.re - takayaki](https://yande.re/wiki/show?title=takayaki)
{ config, ... }:
let
  inherit (config.helpers.overrides) nsfw sfw crop waifu2x;
in
{
  config.moe2nix.danbooru = [
    { id = 7174917; overrides = [ sfw (crop { height = 900; }) ]; }
    { id = 7075305; overrides = [ (crop { height = 1100; }) ]; }
    { id = 7046716; overrides = [ (crop { height = 1500; }) ]; } # yande.re 1143296
    { id = 7008850; overrides = [ (crop { height = 1100; }) ]; }
    { id = 6974586; overrides = [ (crop { height = 1100; }) ]; }
    { id = 6876748; overrides = [ (crop { height = 1100; }) ]; } # yande.re 1132901
    { id = 5743504; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5722414; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5118457; overrides = [ (crop { height = 1100; }) ]; }
    { id = 5071255; overrides = [ sfw (crop { height = 1600; }) ]; } # yande.re 916298
    { id = 5030872; overrides = [ sfw (crop { height = 3000; }) ]; }
    { id = 4625555; overrides = [ (crop { height = 2500; }) ]; } # yande.re 811120
    { id = 4446902; overrides = [ (crop { width = 2975; height = 2300; left = 225; top = 150; }) (waifu2x { noise = true; }) ]; } # yande.re 757694
    { id = 4328757; overrides = [ sfw (crop { height = 900; }) ]; }
    { id = 3918933; overrides = [ (crop { height = 2500; }) ]; } # yande.re 638396
    { id = 3800678; overrides = [ (waifu2x { noise = true; }) ]; } # yande.re 611875
    { id = 3567177; overrides = [ (crop { height = 1000; }) ]; } # yande.re 568164
    3364641 # yande.re 586578
    3364640 # yande.re 586583
    { id = 3364317; overrides = [ (crop { height = 1200; }) ]; }
    { id = 2895330; overrides = [ (crop { height = 1362; }) ]; } # yande.re 414903
    2692142 # yande.re 390014
    2138620 # yande.re 332472
    { id = 2015124; overrides = [ (crop { height = 3000; }) (waifu2x { noise = true; }) ]; }
    { id = 1723671; overrides = [ sfw (crop { height = 2000; }) (waifu2x { noise = true; }) ]; } # yande.re 290754
    1608990 # yande.re 279953
    1105936 # yande.re 208243
    895966 # yande.re 178345
    778523 # yande.re 123172
  ];

  config.moe2nix."yande.re" = [
    { id = 1199948; overrides = [ (crop { height = 3000; }) ]; }
    { id = 1182332; overrides = [ (crop { height = 3000; }) ]; }
    { id = 1156735; overrides = [ sfw (crop { height = 2500; }) ]; }
    1099228
    { id = 896802; overrides = [ (crop { height = 2500; }) ]; }
    877016
    { id = 718678; overrides = [ (crop { width = 2520; height = 2500; left = 188; top = 124; }) (waifu2x { noise = true; }) ]; }
    632571 # danbooru 3626447
    { id = 632570; overrides = [ sfw ]; }
    586576 # danbooru 2057804
    { id = 586570; overrides = [ (crop { height = 1400; }) ]; }
    { id = 517903; overrides = [ nsfw ]; }
    { id = 459396; overrides = [ (crop { height = 2000; }) ]; }
    332473
    332387
    279888
    { id = 195878; overrides = [ nsfw (crop { height = 2500; }) ]; }
  ];
}
