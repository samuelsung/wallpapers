# ねぶそく
# - [danbooru - nebusoku](https://danbooru.donmai.us/artists/123343)
# - [yande.re - nebusoku](https://yande.re/wiki/show?title=nebusoku)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop format rotate sfw;
in
{
  config.moe2nix.danbooru = [
    { id = 7656357; overrides = [ sfw (format "png") (crop { height = 1300; }) ]; }
    { id = 7392102; overrides = [ (rotate 270) ]; }
    { id = 7118983; overrides = [ (crop { height = 2500; }) ]; }
    { id = 6624007; overrides = [ (crop { height = 2000; }) ]; }
    { id = 6596733; overrides = [ (crop { height = 2300; }) ]; }
    { id = 6410705; overrides = [ (crop { height = 2200; }) ]; }
    6008130 # yande.re 1057553
    { id = 5983705; overrides = [ (crop { height = 2200; }) ]; }
    { id = 5577520; overrides = [ (crop { height = 2500; }) ]; }
    { id = 5577519; overrides = [ (crop { height = 2500; }) ]; }
    { id = 5383649; overrides = [ (crop { height = 3000; }) ]; }
    { id = 5288189; overrides = [ (crop { height = 1200; }) ]; }
    { id = 4911230; overrides = [ (crop { height = 931; top = 131; }) ]; } # yande.re 913110
    { id = 4437840; overrides = [ (crop { height = 3000; }) ]; } # yande.re 761866
    { id = 4437838; overrides = [ (crop { height = 2500; }) ]; } # yande.re 761865
    4424698 # yande.re 913114
    { id = 4269715; overrides = [ (crop { height = 2800; }) ]; } # yande.re 719638
    { id = 3731769; overrides = [ (crop { height = 1000; }) ]; }
    { id = 3704172; overrides = [ (crop { height = 1200; }) ]; } # yande.re 591176
    3630784 # yande.re 591176
    { id = 3528819; overrides = [ (crop { height = 2200; }) ]; } # yande.re 543980
    { id = 3340670; overrides = [ (crop { height = 1500; }) ]; } # yande.re 499931
    { id = 3076546; overrides = [ (crop { height = 1200; }) ]; } # yande.re 444756
    { id = 3057577; overrides = [ sfw (crop { height = 1300; }) ]; } # yande.re 441013
    { id = 3057575; overrides = [ (crop { height = 1000; }) ]; } # yande.re 441012
  ];

  config.moe2nix."yande.re" = [
    589452 # danbooru 3689408
  ];
}
