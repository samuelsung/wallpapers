# Ayataka
# - [danbooru - ayataka](https://danbooru.donmai.us/artists/108890)
# - [yande.re - ayataka](https://yande.re/wiki/show?title=ayataka)
{ config, ... }:
let
  inherit (config.helpers.overrides) sfw nsfw crop rotate waifu2x;
in
{
  config.moe2nix.danbooru = [
    { id = 7360162; overrides = [ (crop { height = 2200; }) ]; }
    7316976
    { id = 7316350; overrides = [ sfw (crop { height = 1000; }) ]; }
    7074653
    { id = 7070122; overrides = [ (crop { height = 1500; }) ]; }
    { id = 7049235; overrides = [ sfw (crop { height = 1300; }) (waifu2x { noise = true; }) ]; }
    { id = 6502456; overrides = [ (crop { height = 1850; }) ]; } # yande.re 1105949
    6073380 # yande.re 1064870
    6049875
    { id = 6005438; overrides = [ sfw ]; }
    { id = 5996370; overrides = [ (crop { height = 2000; }) ]; } # yande.re 1056492
    5406746
    { id = 5269636; overrides = [ (crop { height = 1900; }) ]; }
    4755446 # yande.re 832444
    4738429 # yande.re 840582
    { id = 4685254; overrides = [ sfw (crop { height = 1200; }) ]; } # yande.re 774408
    { id = 4594995; overrides = [ (crop { height = 1600; }) ]; } # yande.re 794026
    { id = 4370170; overrides = [ (crop { height = 1100; }) ]; }
    4357290 # yande.re 743282
    4302726 # yande.re 717703
    { id = 4258969; overrides = [ sfw ]; }
    { id = 4225600; overrides = [ sfw ]; }
    { id = 4151946; overrides = [ (crop { height = 1550; }) ]; } # yande.re 694861
    { id = 4130257; overrides = [ sfw ]; } # yande.re 690072
    { id = 3996676; overrides = [ sfw ]; } # yande.re 659491
    { id = 3974810; overrides = [ (crop { height = 1700; }) ]; } # yande.re 653968
    { id = 3524427; overrides = [ sfw (crop { height = 1650; }) ]; }
    { id = 3399739; overrides = [ sfw ]; } # yande.re 512769
  ];

  config.moe2nix."yande.re" = [
    { id = 664683; overrides = [ (crop { height = 2650; }) ]; }
    { id = 594536; overrides = [ (crop { height = 1950; }) ]; }
    582003
    { id = 570208; overrides = [ sfw (crop { height = 1800; }) ]; }
    557600
    507987
    { id = 469380; overrides = [ nsfw (crop { height = 1850; }) ]; }
    440400
  ];
}
