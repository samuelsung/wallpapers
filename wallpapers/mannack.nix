# まんなく
# - [danbooru - mannack](https://danbooru.donmai.us/artists/185680)
# - [yande.re - mannack](https://yande.re/wiki/show?title=mannack)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop extent fill waifu2x sfw nsfw rotate rect;
in
{
  config.moe2nix.danbooru = [
    { id = 8720933; overrides = [ (crop { height = 2200; }) ]; }
    { id = 8493040; overrides = [ (crop { height = 2100; }) ]; }
    8493032 # yande.re 1207886
    { id = 8483302; overrides = [ (extent { background = "#292727"; width = 4000; }) ]; }
    { id = 8176602; overrides = [ (crop { height = 1950; }) ]; }
    { id = 7952843; overrides = [ (crop { height = 1300; }) ]; }
    { id = 7952078; overrides = [ (crop { height = 1850; }) ]; }
    { id = 7808090; overrides = [ (crop { height = 2400; }) ]; }
    { id = 7780743; overrides = [ (crop { height = 2600; }) ]; }
    { id = 7759962; overrides = [ (crop { height = 2050; }) ]; } # yande.re 1216027
    { id = 7740127; overrides = [ (crop { height = 1700; }) ]; }
    7711393 # yande.re 1177568
    { id = 7695422; overrides = [ (crop { height = 2050; }) ]; } # yande.re 1177255
    { id = 7661092; overrides = [ (crop { height = 1900; }) ]; }
    { id = 7646471; overrides = [ (crop { height = 1050; }) ]; }
    { id = 7638272; overrides = [ (crop { height = 2600; }) ]; } # yande.re 1177569
    {
      id = 7585923;
      overrides = [
        (waifu2x { scale = 4; })
        (fill {
          background = "#f2dbd5";
          fuzz = 7;
          starts = [
            { x = 0; y = 0; }
            { x = 357 * 4; y = 474 * 4; }
            { x = 375 * 4; y = 519 * 4; }
            { x = 389 * 4; y = 525 * 4; }
            { x = 381 * 4; y = 546 * 4; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 1300 * 4; gravity = "east"; })
        (crop { height = 1200 * 4; })
      ];
    }
    { id = 7475965; overrides = [ (crop { height = 1400; }) ]; }
    7133255 # yande.re 1147562
    { id = 7013007; overrides = [ (crop { height = 2200; }) ]; }
    { id = 6993100; overrides = [ (crop { height = 1450; }) ]; }
    { id = 6296849; overrides = [ (crop { height = 850; }) ]; }
    { id = 6270448; overrides = [ (crop { height = 1150; }) ]; }
    { id = 6211081; overrides = [ (crop { height = 1200; }) ]; }
    { id = 6164954; overrides = [ (crop { height = 750; }) ]; }
    { id = 6070126; overrides = [ (crop { height = 750; }) ]; }
    { id = 6067694; overrides = [ (crop { height = 1400; }) ]; }
    { id = 5989861; overrides = [ (crop { height = 900; }) ]; }
    { id = 5166033; overrides = [ (crop { height = 3300; }) ]; } # yande.re 936322
    { id = 5021967; overrides = [ (crop { height = 3300; }) ]; } # yande.re 906907
    {
      id = 4940043; # yande.re 881973
      overrides = [
        (extent { background = "#ffffff"; width = 6000; gravity = "west"; })
      ];
    }
    4920825 # yande.re 885160
    { id = 4920735; overrides = [ (crop { height = 4000; }) (waifu2x { noise = true; }) ]; } # yande.re 885136
    4897137 # yande.re 879655
    { id = 4892244; overrides = [ (crop { height = 4000; }) ]; } # yande.re 878796 
    { id = 4891525; overrides = [ (crop { height = 2500; }) ]; } # yande.re 878738 
    { id = 4781202; overrides = [ (crop { height = 900; }) (waifu2x { noise = true; }) ]; }
    4667910
    {
      id = 4617895;
      overrides = [
        (waifu2x { scale = 3; })
        (fill {
          background = "#f2dbd5";
          fuzz = 2;
          starts = [
            { x = 0; y = 0; }
            { x = 3 * 220; y = 3 * 249; }
            { x = 3 * 221; y = 3 * 291; }
            { x = 3 * 233; y = 3 * 421; }
            { x = 3 * 242; y = 3 * 339; }
            { x = 3 * 205; y = 3 * 599; }
            { x = 3 * 221; y = 3 * 620; }
            { x = 3 * 238; y = 3 * 624; }
            { x = 3 * 259; y = 3 * 655; }
            { x = 3 * 258; y = 3 * 670; }
            { x = 3 * 505; y = 3 * 649; }
            { x = 3 * 538; y = 3 * 578; }
            { x = 3 * 537; y = 3 * 618; }
            { x = 3 * 579; y = 3 * 587; }
            { x = 3 * 581; y = 3 * 617; }
            { x = 3 * 568; y = 3 * 257; }
            { x = 3 * 555; y = 3 * 287; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 3 * 1400; })
        (crop { height = 3 * 900; })
      ];
    }
    { id = 4477283; overrides = [ (crop { height = 3200; }) ]; } # yande.re 772577
    {
      id = 4463373;
      overrides = [
        sfw
        (extent { background = "#ffffff"; width = 1200; })
        (crop { height = 800; })
      ];
    }
    {
      id = 4415597;
      overrides = [
        sfw
        (extent { background = "#ffffff"; width = 1000; })
        (crop { height = 1000; })
      ];
    }
    { id = 4412374; overrides = [ nsfw (crop { height = 837; top = 100; }) ]; }
    { id = 4388332; overrides = [ (crop { height = 800; }) ]; }
    { id = 4302743; overrides = [ (crop { width = 3044; height = 2500; left = 100; top = 340; }) ]; }
    {
      id = 4298336;
      overrides = [
        (extent { background = "#ffffff"; width = 1300; })
        (crop { height = 800; top = 136; })
      ];
    }
    { id = 4250651; overrides = [ (crop { height = 700; }) ]; }
    4164640
    { id = 4160510; overrides = [ (crop { height = 1300; }) ]; }
    {
      id = 4137407;
      overrides = [
        (extent { background = "#ffffff"; width = 1400; gravity = "east"; })
        (crop { height = 1200; })
      ];
    }
    { id = 4096883; overrides = [ (crop { height = 1300; }) (waifu2x { noise = true; }) ]; } # yande.re 682781
    { id = 3916104; overrides = [ (rotate 90) ]; }
    { id = 3884020; overrides = [ (crop { height = 3400; }) (waifu2x { noise = true; }) ]; }
    { id = 3841699; overrides = [ nsfw (crop { height = 1500; }) (waifu2x { noise = true; }) ]; }
    {
      id = 3806600;
      overrides = [
        (extent { background = "#ffffff"; width = 5000; })
        (crop { height = 3400; })
      ];
    }
    {
      id = 3801457;
      overrides = [
        (extent { background = "#ffffff"; width = 5000; })
        (crop { height = 3587; top = 1000; })
      ];
    }
    {
      id = 3800395;
      overrides = [
        (extent { background = "#ffffff"; width = 6000; gravity = "east"; })
      ];
    }
    { id = 3776927; overrides = [ (waifu2x { noise = true; }) ]; } # yande.re 608617
    { id = 3764100; overrides = [ (extent { background = "#ffffff"; width = 3300; gravity = "east"; }) ]; }
    { id = 3689821; overrides = [ (crop { height = 3352; top = 1200; }) (waifu2x { noise = true; }) ]; }
  ];

  config.moe2nix."konachan.com" = [
    360594 # danbooru 6535659
    360593 # dnabooru 6169129
    { id = 360592; overrides = [ (crop { height = 4200; }) ]; } # danbooru 6162416
    { id = 360591; overrides = [ nsfw ]; } # danbooru 6044946
    { id = 344665; overrides = [ nsfw ]; } # danbooru 4042215
    344664 # danbooru 3949112
    { id = 344663; overrides = [ nsfw ]; } # danbooru 3909245
  ];

  config.moe2nix."yande.re" = [
    1174767 # danbooru 6835609
    {
      id = 1162171; # danbooru 7376448
      overrides = [
        (crop { height = 5893; top = 300; })
        (extent { background = "#f2dbd5"; width = 8000; })
      ];
    }
    {
      id = 1162169; # danbooru 7280808
      overrides = [
        (crop { height = 4000; })
        (extent { background = "#ffffff"; width = 6000; })
      ];
    }
    {
      id = 1162167; # danbooru 7347546
      overrides = [
        (fill {
          background = "#f2dbd5";
          fuzz = 4;
          starts = [
            { x = 0; y = 0; }
            { x = 3026; y = 1392; }
            { x = 2678; y = 2102; }
            { x = 1214; y = 1698; }
            { x = 1402; y = 1782; }
            { x = 1322; y = 1830; }
            { x = 3144; y = 3496; }
            { x = 2720; y = 3664; }
            { x = 1038; y = 3490; }
            { x = 1238; y = 3522; }
            { x = 1360; y = 4000; }
            { x = 1352; y = 4252; }
            { x = 1336; y = 4452; }
          ];
        })
        (crop { height = 5500; })
        (extent { background = "#f2dbd5"; width = 7000; })
      ];
    }
    {
      id = 1162166;
      overrides = [
        (fill {
          background = "#f2dbd5";
          fuzz = 4;
          starts = [
            { x = 0; y = 0; }
            { x = 2648; y = 1560; }
            { x = 2346; y = 1910; }
            { x = 2255; y = 2495; }
            { x = 1883; y = 1859; }
            { x = 1821; y = 1836; }
            { x = 1245; y = 436; }
            { x = 1822; y = 1767; }
            { x = 1983; y = 3174; }
            { x = 2358; y = 3463; }
            { x = 2570; y = 3937; }
            { x = 3248; y = 3067; }
            { x = 3484; y = 3643; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 7000; })
      ];
    }
    {
      id = 1152278; # danbooru 7138036
      overrides = [
        (fill {
          background = "#f2dbd5";
          fuzz = 4;
          starts = [
            { x = 0; y = 0; }
            { x = 1434; y = 2870; }
            { x = 2136; y = 2376; }
            { x = 2124; y = 2474; }
            { x = 2282; y = 2792; }
            { x = 2496; y = 2874; }
            { x = 3104; y = 2792; }
            { x = 3332; y = 2920; }
            { x = 3536; y = 2928; }
            { x = 3890; y = 5520; }
            { x = 4374; y = 5432; }
            { x = 4330; y = 5540; }
            { x = 4448; y = 5596; }
            { x = 1270; y = 5664; }
            { x = 1098; y = 5256; }
            { x = 1674; y = 5212; }
            { x = 1598; y = 5294; }
            { x = 1472; y = 5462; }
            { x = 4245; y = 2234; }
            { x = 4355; y = 4643; }
            { x = 4460; y = 4676; }
            { x = 4722; y = 4793; }
            { x = 1903; y = 1013; }
            { x = 1942; y = 1108; }
          ];
        })
        (crop { height = 5790; })
        (extent { background = "#f2dbd5"; width = 9000; gravity = "west"; })
      ];
    }
    { id = 1152277; overrides = [ (crop { height = 4500; }) ]; } # danbooru 6946594
    {
      id = 1147532; # danbooru 6265812
      overrides = [
        (extent { background = "#ffffff"; width = 8500; gravity = "west"; })
      ];
    }
    { id = 1110588; overrides = [ (crop { width = 5500; }) (waifu2x { noise = true; }) ]; } # danbooru 6497143
    { id = 1110582; overrides = [ (crop { height = 2000; top = 806; }) (waifu2x { noise = false; }) ]; } # danbooru 6399157
    { id = 1110579; overrides = [ (crop { height = 5700; }) ]; } # danbooru 6478046
    { id = 1110578; overrides = [ (crop { height = 3400; }) ]; } # danbooru 6548672
    {
      id = 1065359; # danbooru 6071162
      overrides = [
        (extent { background = "#ffffff"; width = 4500; })
        (crop { height = 4000; })
      ];
    }
    {
      id = 1026085; # danbooru 5648221
      overrides = [
        nsfw
        (extent { background = "#ffffff"; width = 4000; })
        (crop { height = 3000; })
      ];
    }
    {
      id = 1026084; # danbooru 6265844
      overrides = [
        (fill {
          background = "#f2dbd5";
          fuzz = 5;
          starts = [
            { x = 0; y = 0; }
            { x = 1419; y = 957; }
            { x = 2733; y = 2019; }
            { x = 2145; y = 2109; }
            { x = 2144; y = 2173; }
            { x = 985; y = 2580; }
            { x = 868; y = 2684; }
            { x = 2694; y = 3405; }
            { x = 2831; y = 3482; }
            { x = 3453; y = 3396; }
            { x = 3273; y = 3615; }
            { x = 2838; y = 3370; }
            { x = 1728; y = 1885; }
            { x = 1698; y = 1916; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 7000; })
      ];
    }
    { id = 1016036; overrides = [ (crop { height = 3200; }) ]; } # danbooru 5510472
    1016035 # danbooru 5613058
    { id = 984459; overrides = [ (crop { height = 3300; }) ]; } # danbooru 5405493
    { id = 984458; overrides = [ (crop { height = 3300; top = 1092; }) ]; }
    { id = 977991; overrides = [ (crop { height = 3100; }) ]; } # danbooru 5390610
    {
      id = 977093; # danbooru 5357342
      overrides = [
        (extent { background = "#ffffff"; width = 4500; gravity = "west"; })
        (crop { height = 3300; })
      ];
    }
    { id = 969365; overrides = [ nsfw (extent { background = "#ffffff"; width = 6000; }) ]; } # danbooru 5342328
    {
      id = 960548; # danbooru 5249552
      overrides = [
        (fill {
          background = "#f2dbd5";
          fuzz = 4;
          starts = [
            { x = 0; y = 0; }
            { x = 1015; y = 1030; }
            { x = 1020; y = 1244; }
            { x = 1020; y = 1438; }
            { x = 1145; y = 1552; }
            { x = 1211; y = 1621; }
            { x = 1257; y = 1509; }
            { x = 2484; y = 1542; }
            { x = 2497; y = 1174; }
            { x = 2925; y = 3463; }
            { x = 2778; y = 3435; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 6000; })
        (crop { height = 4000; })
      ];
    }
    { id = 960547; overrides = [ (crop { height = 3000; }) ]; } # danbooru 5285772
    { id = 960545; overrides = [ (crop { height = 3300; }) ]; } # danbooru 5288256
    {
      id = 950175; # danbooriu 4693108 
      overrides = [
        (extent { background = "#ffffff"; width = 5500; })
        (crop { height = 3500; })
      ];
    }
    {
      id = 950174; # danbooru 4816178 
      overrides = [
        (extent { background = "#ffffff"; width = 5500; gravity = "east"; })
        (crop { height = 4200; })
      ];
    }
    { id = 950171; overrides = [ (extent { background = "#ffffff"; width = 4500; }) ]; } # danbooru 4769119 
    {
      id = 950170; # danbooru 4791047 
      overrides = [
        (extent { background = "#ffffff"; width = 4500; gravity = "west"; })
        (crop { height = 4500; })
      ];
    }
    { id = 947912; overrides = [ (crop { height = 3000; }) ]; } # danbooru 5221121
    { id = 947911; overrides = [ (extent { background = "#ffffff"; width = 6000; }) ]; } # danbooru 5221116
    { id = 944706; overrides = [ (rotate 90) (crop { height = 3500; }) ]; } # danbooru 5204037
    {
      id = 934138; # danbooru 5154940
      overrides = [
        (extent { background = "#ffffff"; width = 5000; })
        (crop { height = 3500; })
      ];
    }
    {
      id = 914136; # danbooru 5062006
      overrides = [
        (fill {
          background = "#f2dbd5";
          fuzz = 2;
          starts = [
            { x = 0; y = 0; }
            { x = 1011; y = 939; }
            { x = 1123; y = 1416; }
            { x = 1207; y = 1470; }
            { x = 1282; y = 1519; }
            { x = 1307; y = 1604; }
            { x = 1238; y = 1896; }
            { x = 856; y = 3282; }
            { x = 739; y = 3537; }
            { x = 2152; y = 2955; }
            { x = 2926; y = 3639; }
            { x = 3064; y = 3467; }
            { x = 3097; y = 3948; }
            { x = 3129; y = 4038; }
            { x = 3270; y = 3654; }
            { x = 2835; y = 945; }
            { x = 2927; y = 1217; }
            { x = 2744; y = 1347; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 5000; })
        (crop { height = 4000; })
      ];
    }
    {
      id = 909419; # danbooru 5036769 
      overrides = [
        (extent { background = "#ffffff"; width = 5000; })
        (crop { height = 3000; })
      ];
    }
    {
      id = 902417; # danbooru 4996752 
      overrides = [
        (extent { background = "#0b1115"; height = 5000; })
      ];
    }
    {
      id = 899898; # danbooru 4983655 
      overrides = [
        (rect { color = "#ffffff"; width = 745; height = 623; left = 47; top = 1089; })
        (rect { color = "#ffffff"; width = 600; height = 776; left = 120; top = 965; })
        (rect { color = "#ffffff"; width = 590; height = 627; left = 2691; top = 475; })
        (fill {
          background = "#f2dbd5";
          fuzz = 4;
          starts = [
            { x = 0; y = 0; }
            { x = 852; y = 907; }
            { x = 1023; y = 1377; }
            { x = 1032; y = 1718; }
            { x = 2524; y = 803; }
            { x = 2446; y = 1435; }
            { x = 2404; y = 1617; }
            { x = 1158; y = 1703; }
            { x = 1009; y = 2716; }
            { x = 1159; y = 2871; }
            { x = 1159; y = 3084; }
            { x = 1072; y = 3165; }
            { x = 2249; y = 2748; }
            { x = 2479; y = 2526; }
            { x = 2489; y = 2643; }
            { x = 2630; y = 2502; }
            { x = 2620; y = 2421; }
            { x = 2674; y = 2484; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 5000; })
        (crop { height = 3500; })
      ];
    }
    { id = 882130; overrides = [ (crop { height = 3000; }) ]; } # danbooru 4087463
    { id = 864504; overrides = [ (crop { height = 3000; }) ]; } # danbooru 4848201 
    { id = 807501; overrides = [ (crop { height = 2500; }) ]; } # danbooru 4610581 
    { id = 619079; overrides = [ (crop { height = 3500; }) ]; } # danbooru 3823526
    {
      id = 598044;
      overrides = [
        (extent { background = "#ffffff"; width = 4000; gravity = "west"; })
        (waifu2x { noise = true; })
      ];
    }
    { id = 597139; overrides = [ (crop { height = 4000; }) (waifu2x { noise = true; }) ]; }
  ];

  config.moe2nix.gelbooru = [
    {
      id = 11294784;
      overrides = [
        (waifu2x { scale = 2; })
        (fill {
          background = "#f2dbd5";
          fuzz = 4;
          starts = [
            { x = 0; y = 0; }
            { x = 2 * 525; y = 2 * 1635; }
            { x = 2 * 525; y = 2 * 2109; }
            { x = 2 * 556; y = 2 * 2248; }
            { x = 2 * 575; y = 2 * 1776; }
            { x = 2 * 732; y = 2 * 2264; }
            { x = 2 * 1431; y = 2 * 1932; }
            { x = 2 * 1517; y = 2 * 1847; }
            { x = 2 * 1577; y = 2 * 1607; }
            { x = 2 * 1359; y = 2 * 1149; }
            { x = 2 * 1519; y = 2 * 1234; }
            { x = 2 * 1417; y = 2 * 1107; }
            { x = 2 * 993; y = 2 * 2580; }
            { x = 2 * 885; y = 2 * 2658; }
            { x = 2 * 1392; y = 2 * 2556; }
            { x = 2 * 1896; y = 2 * 2344; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 2 * 3000; })
      ];
    }
    { id = 10679905; overrides = [ (crop { height = 1000; }) ]; }
    { id = 10063426; overrides = [ (extent { background = "#ffffff"; width = 1500; }) (crop { height = 1441; top = 100; }) ]; }
    {
      id = 9406061;
      overrides = [
        (fill {
          background = "#f2dbd5";
          fuzz = 10;
          starts = [
            { x = 0; y = 0; }
            { x = 1188; y = 1468; }
            { x = 1284; y = 1450; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 2500; gravity = "east"; })
      ];
    }
    { id = 9369349; overrides = [ (extent { background = "#ffffff"; width = 2000; gravity = "east"; }) ]; }
    {
      id = 9369342;
      overrides = [
        (extent { background = "#ffffff"; width = 3000; gravity = "east"; })
        (crop { height = 1556; })
      ];
    }
    {
      id = 9120476;
      overrides = [
        (waifu2x { scale = 3; })
        (fill {
          background = "#f2dbd5";
          fuzz = 4;
          starts = [
            { x = 0; y = 0; }
            { x = 3 * 485; y = 3 * 561; }
            { x = 3 * 577; y = 3 * 565; }
            { x = 3 * 555; y = 3 * 648; }
            { x = 3 * 550; y = 3 * 722; }
            { x = 3 * 1211; y = 3 * 871; }
            { x = 3 * 1119; y = 3 * 928; }
            { x = 3 * 444; y = 3 * 1353; }
            { x = 3 * 1112; y = 3 * 691; }
            { x = 3 * 1007; y = 3 * 762; }
            { x = 3 * 1160; y = 3 * 474; }
          ];
        })
        (extent { background = "#f2dbd5"; width = 3 * 2500; })
        (crop { height = 3 * 1800; })
      ];
    }
    { id = 9005001; overrides = [ (extent { background = "#ffffff"; width = 2200; }) (crop { height = 1200; }) ]; }
    { id = 7392709; overrides = [ (crop { height = 2000; }) ]; } # danbooru 5450955
    {
      id = 7340990;
      overrides = [
        (extent {
          background = "#ffffff";
          width = 900;
          gravity = "east";
        })
        (extent { background = "#ffffff"; width = 1200; })
        (crop { height = 900; })
      ];
    }
    { id = 7340936; overrides = [ (extent { background = "#ffffff"; width = 3000; }) ]; }
    { id = 6949318; overrides = [ (extent { background = "#ffffff"; width = 1700; gravity = "west"; }) (crop { height = 1400; }) ]; }
    { id = 5843536; overrides = [ (crop { height = 2300; }) ]; } # danbooru 4326017
    { id = 5843528; overrides = [ (crop { height = 1900; }) ]; } # danbooru 4326025
  ];
}
