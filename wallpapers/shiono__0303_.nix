# しおの
# - [danbooru - shiono_(0303)](https://danbooru.donmai.us/artists/177712)
# - [yande.re - siooooono](https://yande.re/wiki/show?title=siooooono)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop sfw nsfw;
in
{
  config.moe2nix.danbooru = [
    { id = 8311866; overrides = [ (crop { height = 1152; }) ]; }
    7311764 # yande.re 1157768
    6606928 # yande.re 1108770
    { id = 6487268; overrides = [ (crop { height = 2370; }) ]; } # yande.re 1104791
    { id = 3300518; overrides = [ sfw (crop { height = 2500; }) ]; }
  ];

  config.moe2nix."yande.re" = [
    1198295 # danbooru 8246942
    1192048 # danbooru 8070942
    1156394
    1156394
    { id = 1138581; overrides = [ (crop { height = 2900; }) ]; }
    1138575 # danbooru 7165321
    1138574
    1118388 # danbooru 6804155
    1118387
    { id = 841790; overrides = [ nsfw ]; } # danbooru 4747489
    { id = 672895; overrides = [ nsfw ]; }
  ];
}
