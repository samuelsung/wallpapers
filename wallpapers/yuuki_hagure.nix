# 憂姫はぐれ
# - [anbooru - yuuki_hagure](https://danbooru.donmai.us/artists/5546)
# - [yande.re - yuuki_hagure](https://yande.re/wiki/show?title=yuuki_hagure)
{ config, ... }:
let
  inherit (config.helpers.overrides) crop sfw waifu2x;
in
{
  config.moe2nix.danbooru = [
    8514419
    { id = 8237212; overrides = [ (crop { width = 2292; height = 2700; left = 72; }) ]; } # yande.re 1197958
    { id = 8237211; overrides = [ sfw ]; } # yande.re 1197959
    { id = 8237210; overrides = [ (crop { height = 3000; }) ]; } # yande.re 1197960
    { id = 8237209; overrides = [ (crop { height = 2400; }) ]; } # yande.re 1197961
    { id = 8237208; overrides = [ sfw (crop { height = 1600; }) ]; } # yande.re 1197962
    { id = 8237206; overrides = [ sfw (crop { height = 2000; }) ]; } # yande.re 1197963
    { id = 8237205; overrides = [ (crop { height = 2300; }) ]; } # yande.re 1197964
    { id = 8237204; overrides = [ (crop { height = 2200; }) ]; } # yande.re 1197965
    { id = 8237203; overrides = [ (crop { height = 2500; }) ]; } # or danbooru 7963740 yande.re 1197967
    8237202 # yande.re 1197966
    { id = 8140053; overrides = [ (crop { height = 600; }) ]; }
    { id = 8047703; overrides = [ (crop { height = 1200; }) ]; }
    { id = 7288964; overrides = [ (crop { height = 1000; }) ]; }
    { id = 7034909; overrides = [ (crop { height = 1000; }) ]; }
    { id = 6981588; overrides = [ (crop { height = 2600; }) ]; }
    { id = 6981581; overrides = [ (crop { height = 1040; }) ]; }
    6886765
    { id = 6508172; overrides = [ (crop { width = 2378; height = 1340; left = 90; top = 85; }) ]; }
    6466119
    { id = 6052769; overrides = [ sfw (crop { height = 2300; }) ]; } # yande.re 1198105
    { id = 6052641; overrides = [ (crop { height = 2800; }) ]; } # yande.re 1198107
    { id = 6052640; overrides = [ (crop { height = 2800; }) ]; } # yande.re 1198110
    { id = 6052639; overrides = [ (crop { height = 2800; }) ]; } # yande.re 1198111
    { id = 6052638; overrides = [ (crop { width = 2296; height = 2300; left = 56; }) ]; } # yande.re 1198113
    { id = 6052636; overrides = [ sfw ]; } # yande.re 1198116
    { id = 6052633; overrides = [ sfw (crop { width = 2280; height = 2400; left = 64; }) ]; } # yande.re 1198119
    { id = 5980631; overrides = [ (crop { height = 1300; }) ]; } # yande.re 1055177
    { id = 5834448; overrides = [ sfw (crop { height = 2200; }) ]; } # yande.re 1035059
    { id = 5338800; overrides = [ sfw (crop { width = 2532; height = 2200; left = 320; top = 100; }) ]; } # yande.re 967026
    { id = 5191887; overrides = [ sfw (crop { height = 2800; }) ]; } # yande.re 1198070
    { id = 5191836; overrides = [ sfw (crop { width = 2292; height = 2200; left = 32; top = 72; }) ]; } # yande.re 1198072 or danbooru 4656397
    { id = 5191828; overrides = [ sfw (crop { height = 2500; }) ]; } # yande.re 1198073 or danbooru 4267907
    { id = 5191822; overrides = [ (crop { width = 2284; height = 3000; left = 72; }) ]; } # yande.re 5191822
    { id = 5134234; overrides = [ sfw (crop { height = 700; }) ]; }
    { id = 4716677; overrides = [ (crop { height = 2000; }) ]; } # yande.re 824442
    { id = 4299916; overrides = [ (crop { height = 1400; }) ]; }
    4130155 # yande.re 690048
    { id = 4110366; overrides = [ (crop { height = 2500; }) ]; } # yande.re 685795
    { id = 4061733; overrides = [ (crop { height = 2800; }) ]; } # yande.re 676450
    { id = 4006875; overrides = [ (crop { height = 2800; }) ]; } # yande.re 704181 or danbooru 2957311
    3963050
    { id = 3920765; overrides = [ (crop { height = 2500; }) ]; } # yande.re 636466
    { id = 3920763; overrides = [ sfw (crop { height = 2500; }) ]; } # yande.re 636467
    { id = 3920762; overrides = [ sfw (crop { height = 2800; }) ]; } # yande.re 636476
    { id = 3910856; overrides = [ (crop { height = 900; }) ]; }
    { id = 3910644; overrides = [ (crop { height = 2500; }) ]; } # yande.re 636469
    { id = 3825660; overrides = [ (crop { height = 2500; }) ]; }
    3818901 # yande.re 617790
    { id = 3463932; overrides = [ (crop { width = 3966; height = 2793; left = 63; top = 66; }) ]; } # yande.re 530019
    { id = 3363042; overrides = [ (crop { height = 1700; }) ]; } # yande.re 503599
    { id = 3219581; overrides = [ (crop { height = 2500; }) ]; }
    { id = 3219579; overrides = [ (crop { width = 3042; left = 381; }) ]; }
    { id = 3215149; overrides = [ (crop { height = 3000; }) ]; } # yande.re 427643
    { id = 3191808; overrides = [ (crop { width = 1944; height = 2048; left = 90; top = 69; }) ]; } # yande.re 466491
    { id = 2968762; overrides = [ (crop { height = 1600; }) ]; } # yande.re 426221
    { id = 2906519; overrides = [ (crop { height = 1500; }) ]; } # yande.re 416124
    { id = 2853064; overrides = [ (crop { height = 2200; }) ]; } # yande.re 409456
    { id = 2837850; overrides = [ sfw (crop { height = 2000; }) ]; } # yande.re 407202
    { id = 2644160; overrides = [ (crop { height = 2000; }) ]; } # yande.re 385261
    2591491 # yande.re 380169
    { id = 2580962; overrides = [ sfw (crop { height = 1500; }) ]; } # yande.re 379150
    { id = 2512325; overrides = [ sfw (crop { height = 5500; }) ]; } # yande.re 378168
    { id = 2350437; overrides = [ (crop { height = 4000; }) ]; } # yande.re 354000
    { id = 2223548; overrides = [ (crop { height = 1700; }) ]; } # yande.re 341895
    2216258
    { id = 2164100; overrides = [ (crop { height = 3500; }) ]; } # yande.re 335500
    { id = 1884956; overrides = [ (crop { height = 2000; }) ]; } # yande.re 307333
    { id = 1833419; overrides = [ (crop { height = 1500; }) ]; } # yande.re 302229
    { id = 1633128; overrides = [ (crop { height = 5500; }) ]; } # yande.re 282250
    1036940
    1009421
  ];

  config.moe2nix."yande.re" = [
    { id = 585739; overrides = [ (crop { height = 2500; }) ]; } # danbooru 3206420
    { id = 479225; overrides = [ (crop { width = 2502; height = 1878; }) ]; } # danbooru 3240658
    { id = 329440; overrides = [ (crop { width = 2438; }) ]; }
    { id = 323914; overrides = [ (crop { height = 1500; }) (waifu2x { noise = true; }) ]; }
    { id = 313834; overrides = [ sfw (crop { height = 1500; }) ]; } # danbooru 1940649
  ];
}
