{ config, ... }:
let
  inherit (config.helpers.overrides) sfw nsfw crop waifu2x;
in
{
  config.moe2nix.danbooru = [
    5090044
    { id = 5105311; overrides = [ sfw ]; }
    5124479
    { id = 5134753; overrides = [ sfw ]; }
    { id = 5169048; overrides = [ sfw ]; }
    5185521
    5230751
    { id = 5243592; overrides = [ sfw ]; }
    { id = 5243595; overrides = [ sfw ]; }
    5243596
    5282469
    { id = 5379969; overrides = [ nsfw ]; }
    5700154
    { id = 5764303; overrides = [ sfw (waifu2x { noise = true; }) ]; }
    5813060
    5847977
    5867871
    { id = 5891582; overrides = [ sfw ]; }
    5919391
    5942715
    6011048
    6019706
    6029240
    6084980
    { id = 6109263; overrides = [ sfw ]; }
    { id = 6183910; overrides = [ sfw ]; }
    6201535
    { id = 6204792; overrides = [ sfw ]; }
    6220066
    6222270
    6231553
    6231556
    6231574
    6235994
    6256117
    { id = 6325154; overrides = [ sfw ]; }
    6425839
    6241510
    6241942
    6259732
    6301811
    6312474
    6320748
    { id = 6379902; overrides = [ sfw ]; }
    6344470
    { id = 6365059; overrides = [ sfw ]; }
    { id = 6379716; overrides = [ sfw ]; }
    { id = 6379773; overrides = [ sfw ]; }
    6389780
    6399391
    6428495
    6450595
    { id = 6452915; overrides = [ sfw ]; }
    { id = 6486271; overrides = [ sfw ]; }
    6493893
    6499436
    6514735
    6530851
    6537067
    6537356
    6537225
    6550739
    6556063
    6566687
    6568702
    6569527
    6572416
    6572568
    6587815
    6587818
    6620271
    6621649
    6638745
    6645396
    6655061
    6657636
    6688545
    6701684
    6706133
    6746028
    6746177
    6767132
    6780682
    6797310
    { id = 6810349; overrides = [ sfw ]; }
    6811386
    6816489
    6818280
    6818936
    6823986
    6830844
    6838408
    6858605
    6865606
    6867141
    6867468
    6867673
    6869012
    6874949
    6892805
    6907425
    { id = 6916342; overrides = [ nsfw ]; }
    6919552
    6932577
    6932673
    6937719
    6949954
    6988258
    6999368
    6999437
    7017481
    7017852
    7032525
    7049583
    7056702 # yande.re 1147574
    7078932
    7089025
    7089807
    7134047
    { id = 7114815; overrides = [ (crop { height = 1185; }) ]; }
    7145592
    7176268
    7194796
    { id = 7220743; overrides = [ nsfw ]; }
    7226220
    7226567
    7226588
    7226896
    7226898
    { id = 7227005; overrides = [ sfw ]; }
    7227233
    7227722
    7232280
    7283243
    7439068
    7725544
    7772387
    8040060
    8141640
    8164235
    8179836
    { id = 8318504; overrides = [ (crop { height = 3200; }) ]; } # yande.re 1147575
    8499440
    8502471
    { id = 8580630; overrides = [ sfw ]; }
  ];

  config.moe2nix."yande.re" = [
    493816
    643690 # danbooru 3809493
    675341
    781400
    899913
    1142339
    1146766
    1147145
    { id = 1147619; overrides = [ nsfw ]; }
    1148112
    1207754
    { id = 1210108; overrides = [ (crop { height = 2500; }) ]; }
  ];

  config.moe2nix."unknown" = [
    { url = "https://heaven-burns-red.com/assets/images/common/fankit/download/wallpaper/pc/vbg_hbr.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-10/ffxiv_dx11%202023-11-10%2021-30-20.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-26/ffxiv_dx11%202023-11-26%2004-44-07.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-10/ffxiv_dx11%202023-11-10%2021-33-25.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-10/ffxiv_dx11%202023-11-10%2021-41-50.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-26/ffxiv_dx11%202023-11-26%2005-02-06.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-12/ffxiv_dx11%202023-11-12%2017-02-15.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-12/ffxiv_dx11%202023-11-12%2017-03-09.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-12/ffxiv_dx11%202023-11-12%2017-04-38.png"; overrides = [ sfw ]; }
    { url = "https://gitea.samuelsung.net/samuelsung/ff14ss/raw/branch/master/ss/2023-11-12/ffxiv_dx11%202023-11-12%2017-15-36.png"; overrides = [ sfw ]; }
  ];
}
