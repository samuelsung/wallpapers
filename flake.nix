{
  description = "wallpaper references";

  inputs = {
    devenv.url = "github:cachix/devenv";
    flake-parts.url = "github:hercules-ci/flake-parts";
    mk-shell-bin.url = "github:rrbutani/nix-mk-shell-bin";
    moe2nix.url = "git+https://codeberg.org/samuelsung/moe2nix";
    nixpkgs.url = "nixpkgs/nixos-unstable";
    nix2container.url = "github:nlewo/nix2container";
    nix2container.inputs.nixpkgs.follows = "nixpkgs";
    notalike.url = "git+https://codeberg.org/samuelsung/notalike";
  };

  outputs = inputs@{ flake-parts, ... }:
    flake-parts.lib.mkFlake { inherit inputs; }
      {
        systems = [ "x86_64-linux" ];
        imports = [
          inputs.moe2nix.flakeModules.default
          ./devenv
          ./helpers
          ./packages
          ./wallpapers
          ./wallpapers.nix
        ];
      };
}
